#ifndef _BACKGROUND_INCLUDE
#define _BACKGROUND_INCLUDE


#include "ShaderProgram.h"


// Given a point (x, y) and a size (width, height) in pixels Background creates 
// a VBO with two triangles. Vertices in the VBO have only a position
// attribute (no colors or texture coordinates)


class Background
{

public:
	// Backgrounds can only be created inside an OpenGL context
	static Background *createBackground(float x, float y, float width, float height, ShaderProgram &program);

	Background(float x, float y, float width, float height, ShaderProgram &program);

	void render() const;
	void free();

private:
	GLuint vao;
	GLuint vbo;
	GLint posLocation;

};


#endif // _Background_INCLUDE

