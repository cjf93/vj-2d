#include "Slug.h"



Slug::Slug()
{
}


Slug::~Slug()
{
}

#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Slug.h"
#include "Game.h"
#include "reader.h"
#include <stdlib.h>


#define JUMP_ANGLE_STEP 4
#define JUMP_HEIGHT 96
#define FALL_STEP 4
#define TAMANO_INVENTARIO 16


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT
};
enum SlugDecisiones
{
	GO_LEFT, GO_RIGHT, JUMP , STAND
};


void Slug::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	bJumping = false;
	spritesheet.loadFromFile("images/slime.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32, 24), glm::vec2(1, 0.5), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(4);

	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 0.f));

	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.f, 0.f));

	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.0f));

	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.0f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.0f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.5f));

	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	speed = 1;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posSlug.x), float(tileMapDispl.y + posSlug.y)));


}

int Slug::tomarDecision(glm::ivec2 posPLayer) {
	int decision= STAND;
	if (posPLayer.x < posSlug.x) decision = GO_LEFT;
	if (posPLayer.x > posSlug.x) decision = GO_RIGHT;
	if (decision == GO_RIGHT && map->collisionMoveRight(posSlug, glm::ivec2(34, 23))) {
		decision = JUMP;
	}
	glm::ivec2 auxPosSlug = posSlug;
	auxPosSlug.x -= speed;
	if (decision == GO_LEFT && map->collisionMoveLeft(auxPosSlug, glm::ivec2(30, 23))) decision = JUMP;
	return decision;
}

void Slug::update(int deltaTime, glm::ivec2 playerPos)
{

	sprite->update(deltaTime);
	int decision = STAND;
	if (glm::abs((playerPos.x + 32 / 2) - (posSlug.x + 32 / 2)) < 20*16){
		if ((glm::abs((playerPos.y + 32 / 2) - (posSlug.y + 32 / 2)) < 10*16)) {
			 decision = tomarDecision(playerPos);
			if (decision == GO_LEFT)
			{
				if (sprite->animation() != MOVE_LEFT)
					sprite->changeAnimation(MOVE_LEFT);
				posSlug.x -= speed;
				if (map->collisionMoveLeft(posSlug, glm::ivec2(32, 24)))
				{
					posSlug.x += speed;
					sprite->changeAnimation(STAND_LEFT);
				}
			}
			else if (decision == GO_RIGHT)
			{
				if (sprite->animation() != MOVE_RIGHT)
					sprite->changeAnimation(MOVE_RIGHT);
				posSlug.x += speed;
				if (map->collisionMoveRight(posSlug, glm::ivec2(32, 24)))
				{
					posSlug.x -= speed;
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
			else
			{
				if (sprite->animation() == MOVE_LEFT)
					sprite->changeAnimation(STAND_LEFT);
				else if (sprite->animation() == MOVE_RIGHT)
					sprite->changeAnimation(STAND_RIGHT);
			}
		}
	}
			if (bJumping)
			{
				jumpAngle += JUMP_ANGLE_STEP;
				if (jumpAngle == 180)
				{
					bJumping = false;
					posSlug.y = startY;
				}
				else
				{
					int auxSlugY = posSlug.y;//pos antes de incrementar la altura (por si colisiono) 
					posSlug.y = int(startY - 50 * sin(3.14159f * jumpAngle / 180.f));
					if (jumpAngle > 90)
						bJumping = !map->collisionMoveDown(posSlug, glm::ivec2(32, 24), &posSlug.y);
					else if (map->collisionMoveUp(posSlug, glm::ivec2(32, 24), &posSlug.y)) {
						jumpAngle = 180 - jumpAngle;
						posSlug.y = auxSlugY;
					}
				}
			}
			else
			{
				posSlug.y += FALL_STEP;
				if (map->collisionMoveDown(posSlug, glm::ivec2(32, 24), &posSlug.y))
				{
					if (decision == JUMP)
					{
						bJumping = true;
						jumpAngle = 0;
						startY = posSlug.y;
					}
				}
			}
	std::pair<bool, bool> mouseStatus;
	mouseStatus = Game::instance().getMouseStatus();
	Game::instance().setStatus(false, false);
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posSlug.x), float(tileMapDispl.y + posSlug.y)));
}



void Slug::render()
{
	sprite->render();
}

void Slug::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Slug::impacto(int hit)
{
	vida = vida - hit;
}

void Slug::setPosition(const glm::vec2 &pos)
{
	posSlug = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posSlug.x), float(tileMapDispl.y + posSlug.y)));
}
glm::vec2 Slug::getPosition()
{
	return posSlug;
}

int Slug::getVida()
{
	return vida;
}


