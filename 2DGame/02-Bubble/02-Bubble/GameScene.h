#ifndef _GAMESCENE_INCLUDE
#define _GAMESCENE_INCLUDE


#include <glm/glm.hpp>
#include "ShaderProgram.h"
#include "TileMap.h"
#include "Player.h"
#include "Slug.h"
#include "InventarioUI.h"
#include "Arpia.h"
#include "HPUI.h"
#include <Windows.h>
#include <MMSystem.h>
#include "Minimapa.h"

// Scene contains all the entities of our game.
// It is responsible for updating and render them.


class GameScene
{

public:
	GameScene();
	~GameScene();

	void init();
	void update(int deltaTime);
	void render();
	int getDespCamX();
	int getDespCamY();
	bool is_Paused();
	void set_Paused(bool b);

private:
	void initShaders();

private:
	Texture Background;
	Sprite *BackgroundSprite;
	TileMap *map;
	Player *player;
	vector<Arpia*> arpias;
	vector<Slug*> slugs;
	InventarioUI *inventario;
	HPUI *hpUI;
	Minimapa *minimapa;
	ShaderProgram texProgram;
	float currentTime;
	glm::mat4 projection;
	int despCamX;
	int despCamY;

	bool Paused;

};


#endif // _SCENE_INCLUDE

