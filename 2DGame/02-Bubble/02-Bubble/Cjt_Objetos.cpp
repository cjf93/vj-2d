#include "Cjt_Objetos.h"



Cjt_Objetos::Cjt_Objetos()
{
}


void Cjt_Objetos::leerObjetos()
{
	reader reader;
	objetos = reader.leerObjetos();
}

objeto Cjt_Objetos::getObjeto(int x)
{
	bool trobat = false;
	for (int i = 0; i < objetos.size() && !trobat; i++) {
		if (objetos[i].getId() == x) {
			return objetos[i];
		}
	}
	return objetos[x];
}

int Cjt_Objetos::getSize()
{
	return objetos.size();
}

Cjt_Objetos::~Cjt_Objetos()
{
}
