#include <iostream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "GameScene.h"
#include "Game.h"

#include <stdio.h>
#include <omp.h>
#include <thread> 
#include  <sys/types.h>




#define SCREEN_X 0
#define SCREEN_Y 0

#define INIT_PLAYER_X_TILES 180
#define INIT_PLAYER_Y_TILES 43


GameScene::GameScene()
{
	map = NULL;
	player = NULL;
	inventario = NULL;
	hpUI = NULL;
}

GameScene::~GameScene()
{
	if(map != NULL)
		delete map;
	if(player != NULL)
		delete player;
	if (inventario != NULL)
		delete inventario;
	if (hpUI != NULL)
		delete inventario;
}



void GameScene::init()
{
	initShaders();
	//PlaySound(TEXT("sounds/background.wav"), NULL, SND_LOOP |SND_ASYNC);
	//Paused = false;
	Cjt_Objetos::instance().leerObjetos();
	Background.loadFromFile("images/Fondo.png", TEXTURE_PIXEL_FORMAT_RGBA);
	BackgroundSprite = Sprite::createSprite(glm::ivec2(4000, 1600), glm::vec2(1.f, 1.f), &Background, &texProgram);
	BackgroundSprite->setNumberAnimations(1);
	BackgroundSprite->setAnimationSpeed(0, 1);
	BackgroundSprite->addKeyframe(0, glm::vec2(0.f, 0.f));
	BackgroundSprite->changeAnimation(0);
	BackgroundSprite->setPosition(glm::vec2(float(0), float(0)));
	//mapa
	map = TileMap::createTileMap("levels/level02.txt", glm::vec2(SCREEN_X, SCREEN_Y), texProgram);
	//slug
	Slug *slug1 = new Slug();
	slug1->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	slug1->setPosition(glm::vec2(30 * map->getTileSize(), 41 * map->getTileSize()));
	slug1->setTileMap(map);
	slugs.push_back(slug1);
	Slug *slug2 = new Slug();
	slug2->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	slug2->setPosition(glm::vec2(72 * map->getTileSize(), 71 * map->getTileSize()));
	slug2->setTileMap(map);
	slugs.push_back(slug2);
	Slug *slug3 = new Slug();
	slug3->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	slug3->setPosition(glm::vec2(220 * map->getTileSize(), 71 * map->getTileSize()));
	slug3->setTileMap(map);
	slugs.push_back(slug3);
	Slug *slug4 = new Slug();
	slug4->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	slug4->setPosition(glm::vec2(220 * map->getTileSize(), 31 * map->getTileSize()));
	slug4->setTileMap(map);
	slugs.push_back(slug4);
	Arpia *arpia1 = new Arpia();
	arpia1->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	arpia1->setPosition(glm::vec2(10 * map->getTileSize(), 26 * map->getTileSize()));
	arpia1->setTileMap(map);
	arpias.push_back(arpia1);
	Arpia *arpia2 = new Arpia();
	arpia2->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	arpia2->setPosition(glm::vec2(200 * map->getTileSize(), 12 * map->getTileSize()));
	arpia2->setTileMap(map);
	arpias.push_back(arpia2);
	//inventarioUI
	inventario = new InventarioUI();
	inventario->init(glm::ivec2(SCREEN_X, SCREEN_Y), glm::ivec2(SCREEN_WIDTH,SCREEN_HEIGHT), texProgram);
	inventario->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	//HP
	hpUI = new HPUI();
	hpUI->init(glm::ivec2(SCREEN_X, SCREEN_Y), glm::ivec2(SCREEN_WIDTH, SCREEN_HEIGHT), texProgram);
	hpUI->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	hpUI->setHP(10);
	//player
	player = new Player();
	player->setInventory(inventario);
	player->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	player->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	player->setTileMap(map);
	player->setHPUI(hpUI);

	minimapa = new Minimapa();
	minimapa->init(glm::ivec2(SCREEN_X, SCREEN_Y), glm::ivec2(SCREEN_WIDTH, SCREEN_HEIGHT), texProgram);
	minimapa->setPosition(glm::vec2(INIT_PLAYER_X_TILES * map->getTileSize(), INIT_PLAYER_Y_TILES * map->getTileSize()));
	projection = glm::ortho(0.f, float(SCREEN_WIDTH - 1), float(SCREEN_HEIGHT - 1), 0.f);
	currentTime = 0.0f;
	despCamX= SCREEN_WIDTH;
	despCamY= SCREEN_HEIGHT;
}

void GameScene::update(int deltaTime)
{
	currentTime += deltaTime;
	if (player->getPosition().x > 4000 - 1024 / 2) {
		despCamX = 4000 - 1024 / 2;

	}
	else if (player->getPosition().x < 1024 / 2)
		despCamX = 0;
	despCamX = player->getPosition().x;
	despCamY = player->getPosition().y;
	BackgroundSprite->update(deltaTime);
	player->update(deltaTime,arpias,slugs);
	for (int i = 0; i < arpias.size(); ++i) arpias[i]->update(deltaTime, player->getPosition());
	for (int i = 0; i < slugs.size(); ++i) slugs[i]->update(deltaTime, player->getPosition());
	inventario->update(deltaTime,player->getPosition());
	hpUI->update(deltaTime, player->getPosition());
	float playerX = player->getPosition().x;
	float playerY = player->getPosition().y;
	despCamX = player->getPosition().x-despCamX;
	despCamY = player->getPosition().y-despCamY;
	minimapa->update(deltaTime, player->getPosition());
	if (playerX < SCREEN_WIDTH/2) {
		projection = glm::ortho(0.f, float(SCREEN_WIDTH) , float(playerY + SCREEN_HEIGHT / 2), float(playerY - SCREEN_HEIGHT / 2));
	}
	else if (playerX > 4000 - SCREEN_WIDTH / 2) {
		projection = glm::ortho(float(4000 - SCREEN_WIDTH), 4000.f, float(playerY + SCREEN_HEIGHT / 2), float(playerY - SCREEN_HEIGHT / 2));
	}
	else projection = glm::ortho(playerX- SCREEN_WIDTH/2, float(playerX +SCREEN_WIDTH/2), float(playerY + SCREEN_HEIGHT/2), float(playerY - SCREEN_HEIGHT/2));
	if (Game::instance().getKey(27)) {
		Paused = true;
		cout << "esc" << endl;
	}
}

void GameScene::render()
{
	glm::mat4 modelview;

	texProgram.use();
	texProgram.setUniformMatrix4f("projection", projection);
	texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
	modelview = glm::mat4(1.0f);
	texProgram.setUniformMatrix4f("modelview", modelview);
	texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);
	BackgroundSprite->render();
	map->render();
	player->render();
	for (int i = 0; i < arpias.size(); ++i)
		arpias[i]->render();
	for (int i = 0; i < slugs.size(); ++i)
		slugs[i]->render();
	inventario->render();
	hpUI->render();
	if (Game::instance().getKey('m'))minimapa->render();
}

int GameScene::getDespCamX()
{
	return despCamX;
}

int GameScene::getDespCamY()
{
	return despCamY;
}

bool GameScene::is_Paused()
{
	return Paused;
}

void GameScene::set_Paused(bool b)
{
	Paused = b;
}

void GameScene::initShaders()
{
	Shader vShader, fShader;

	vShader.initFromFile(VERTEX_SHADER, "shaders/texture.vert");
	if(!vShader.isCompiled())
	{
		cout << "Vertex Shader Error" << endl;
		cout << "" << vShader.log() << endl << endl;
	}
	fShader.initFromFile(FRAGMENT_SHADER, "shaders/texture.frag");
	if(!fShader.isCompiled())
	{
		cout << "Fragment Shader Error" << endl;
		cout << "" << fShader.log() << endl << endl;
	}
	texProgram.init();
	texProgram.addShader(vShader);
	texProgram.addShader(fShader);
	texProgram.link();
	if(!texProgram.isLinked())
	{
		cout << "Shader Linking Error" << endl;
		cout << "" << texProgram.log() << endl << endl;
	}
	texProgram.bindFragmentOutput("outColor");
	vShader.free();
	fShader.free();
}



