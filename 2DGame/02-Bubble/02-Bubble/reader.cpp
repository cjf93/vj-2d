#include "reader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "TileMap.h"
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>

reader::reader()
{
	path = "objetos/objetos.txt";
}



reader::~reader()
{
}

std::vector<objeto> reader::leerObjetos()
{
	
	ifstream file;
	file.open(path.c_str());
	string caracteristica;
	int id;
	int textureId;
	bool usable;
	bool consumible;
	bool crafteable;
	int dureza;
	int ataque;
	bool dropeable;
	bool mineria;
	int objetonecesario;
	int numObjetos;
	vector<objeto> listado(30);
	while (getline(file, caracteristica)){//quitamos el nombre
		getline(file, caracteristica);
		id = atoi(caracteristica.c_str());
		getline(file, caracteristica);
		textureId = atoi(caracteristica.c_str());
		getline(file, caracteristica);
		usable = caracteristica != "0";
		getline(file, caracteristica);
		consumible = caracteristica != "0";
		getline(file, caracteristica);
		crafteable = caracteristica != "0";
		getline(file, caracteristica);
		dureza = atoi(caracteristica.c_str());
		getline(file, caracteristica);
		ataque = atoi(caracteristica.c_str());
		getline(file, caracteristica);
		dropeable = caracteristica != "0";
		getline(file, caracteristica);
		mineria = caracteristica != "0";
		getline(file, caracteristica);
		objetonecesario = atoi(caracteristica.c_str());
		int numDependencias = 0;
		getline(file, caracteristica);
		numDependencias = atoi(caracteristica.c_str());
		vector<pair<int, int> > dependencias;
		for (int i = 0; i < numDependencias; ++i) { // leemos las dependencias de crafteo
			pair<int, int> auxPair;
			getline(file, caracteristica);
			auxPair.first = atoi(caracteristica.c_str());//id
			getline(file, caracteristica);
			auxPair.second = atoi(caracteristica.c_str());//cantidad
			dependencias.push_back(auxPair);
		}
		listado[id]=(objeto(id, textureId, usable, consumible, crafteable, dureza, ataque, dropeable, mineria, objetonecesario, dependencias));
	}
	cout << listado[0].getId() << " " << listado[0].getDureza();
	file.close();
	return listado;
}

