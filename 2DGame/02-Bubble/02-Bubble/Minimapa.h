#ifndef _MINIMAPA_INCLUDE
#define _MINIMAPA_INCLUDE


#include "Sprite.h"
#include "TileMap.h"
#include "objeto.h"
#include "Cjt_Objetos.h"


class Minimapa
{

public:
	void init(const glm::ivec2 &tileMapPos, const glm::ivec2 &mapSize, ShaderProgram & shaderProgram);
	void setPintar(bool pintar);
	void update(int deltaTime, glm::vec2 posPlayer);
	void render();
	void setPosition(const glm::vec2 &pos);
	glm::vec2 getPosition();

private:
	Sprite* createMinimapa( ShaderProgram &shaderProgram);
	Sprite * createCara(ShaderProgram & shaderProgram);
private:
	ShaderProgram shaderProgram;
	Texture miniMapa_texture;
	Sprite *minimapa_sprite;
	Texture cara_texture;
	Sprite *cara_sprite;
	glm::ivec2 tileMapDispl, posMinimapa;
	float xOffset, yOffset;
	bool pintar;
};


#endif // _Minimapa_INCLUDE

