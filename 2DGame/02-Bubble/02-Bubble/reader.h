#ifndef _READER_INCLUDE
#define _READER_INCLUDE

#include "objeto.h"
#include <vector>
#include<string>


class reader
{
public:
	reader();
	std::vector<objeto> leerObjetos();
	~reader();


private:

	std::string path;
};
#endif
