#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Player.h"
#include "Game.h"
#include "reader.h"     
#include "Cjt_Objetos.h"
#include <Windows.h>
#include <MMSystem.h>

#define JUMP_ANGLE_STEP 4
#define JUMP_HEIGHT 96
#define FALL_STEP 4
#define TAMANO_INVENTARIO 16
#define TIME_SHIELD 1500
#define TIME_ACTUAR 500


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT
};

enum PlayerAttackAnims
{
	ATTACK_SWORD1_LEFT, ATTACK_SWORD1_RIGHT, ATTACK_SWORD2_LEFT, ATTACK_SWORD2_RIGHT, MIN_LEFT, MIN_RIGHT, NONE
};


void Player::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	bJumping = false;
	cPulsado = false;
	timeActuar = TIME_ACTUAR;
	timeShield = TIME_SHIELD;
	spawnPoint.first = 180*16;
	spawnPoint.second = 43*16;
	shield = false;
	spritesheet.loadFromFile("images/Player.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32, 48), glm::vec2(0.5, 0.2), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(4);

	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 0.f));

	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.5f, 0.f));

	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.2f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.4f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.6f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.8f));

	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5, 0.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5, 0.2f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5, 0.4f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5, 0.6f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5, 0.8f));

	sprite->changeAnimation(0);

	spritesheetEspada.loadFromFile("images/EspadaAnim.png", TEXTURE_PIXEL_FORMAT_RGBA);
	spriteEspada = Sprite::createSprite(glm::ivec2(104, 77), glm::vec2(0.2, 0.33), &spritesheetEspada, &shaderProgram);
	spriteEspada->setNumberAnimations(7);

	spriteEspada->setAnimationSpeed(ATTACK_SWORD1_LEFT, 24);
	spriteEspada->addKeyframe(ATTACK_SWORD1_LEFT, glm::vec2(0.0f, 0.33f));
	spriteEspada->addKeyframe(ATTACK_SWORD1_LEFT, glm::vec2(0.2f, 0.33));
	spriteEspada->addKeyframe(ATTACK_SWORD1_LEFT, glm::vec2(0.f, 0.66f));
	spriteEspada->addKeyframe(ATTACK_SWORD1_LEFT, glm::vec2(0.f, 0.66f));

	spriteEspada->setAnimationSpeed(ATTACK_SWORD1_RIGHT, 24);
	spriteEspada->addKeyframe(ATTACK_SWORD1_RIGHT, glm::vec2(0.0f, 0.33f));
	spriteEspada->addKeyframe(ATTACK_SWORD1_RIGHT, glm::vec2(0.2f, 0.f));
	spriteEspada->addKeyframe(ATTACK_SWORD1_RIGHT, glm::vec2(0.f, 0.f));
	spriteEspada->addKeyframe(ATTACK_SWORD1_RIGHT, glm::vec2(0.f, 0.f));

	spriteEspada->setAnimationSpeed(ATTACK_SWORD2_LEFT, 24);
	spriteEspada->addKeyframe(ATTACK_SWORD2_LEFT, glm::vec2(0.4f, 0.33f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_LEFT, glm::vec2(0.6f, 0.33f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_LEFT, glm::vec2(0.4f, 0.66f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_LEFT, glm::vec2(0.4f, 0.66f));

	spriteEspada->setAnimationSpeed(ATTACK_SWORD2_RIGHT, 24);
	spriteEspada->addKeyframe(ATTACK_SWORD2_RIGHT, glm::vec2(0.4f, 0.33f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_RIGHT, glm::vec2(0.6f, 0.f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_RIGHT, glm::vec2(0.4f, 0.f));
	spriteEspada->addKeyframe(ATTACK_SWORD2_RIGHT, glm::vec2(0.4f, 0.f));

	spriteEspada->setAnimationSpeed(MIN_LEFT, 24);
	spriteEspada->addKeyframe(MIN_LEFT, glm::vec2(0.8f, 0.0f));
	spriteEspada->addKeyframe(MIN_LEFT, glm::vec2(0.8f, 0.33f));
	spriteEspada->addKeyframe(MIN_LEFT, glm::vec2(0.8f, 0.33f));
	spriteEspada->addKeyframe(MIN_LEFT, glm::vec2(0.8f, 0.66f));

	spriteEspada->setAnimationSpeed(MIN_RIGHT, 24);
	spriteEspada->addKeyframe(MIN_RIGHT, glm::vec2(0.6f, 0.66f));
	spriteEspada->addKeyframe(MIN_RIGHT, glm::vec2(0.2f, 0.66f));
	spriteEspada->addKeyframe(MIN_RIGHT, glm::vec2(0.2f, 0.66f));
	spriteEspada->addKeyframe(MIN_RIGHT, glm::vec2(0.8f, 0.66f));

	spriteEspada->setAnimationSpeed(NONE, 1);
	spriteEspada->addKeyframe(NONE, glm::vec2(0.8f, 0.66f));

	spriteEspada->changeAnimation(NONE);

	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	spriteEspada->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	init_inventario();//iniciamos el inventario
	seleccionado = 0;

}

void Player::init_inventario() {
	inventarioUI->setObjectToInventoryPos(23);
	inventarioUI->setObjectToInventoryPos(15);
	inventarioUI->setObjectToInventoryPos(12);
	inventarioUI->setObjectToInventoryPos(13);
}
bool Player::colisionMonstruo(Slug * slug)
{
	glm::ivec2 slugPos = slug->getPosition();
	if (glm::abs((posPlayer.x + 32 / 2) - (slugPos.x + 32 / 2)) < (31))
		if ((glm::abs((posPlayer.y + 32 / 2) - (slugPos.y + 32 / 2)) < (31 / 2 + 24 / 2))) {
			PlaySound(TEXT("sounds/hit2.wav"), NULL, SND_ASYNC);
			return true;
		}
	return false;
}
bool Player::colisionMonstruo(Arpia * arpia)
{
	glm::ivec2 arpiaPos = arpia->getPosition();
	if (glm::abs((posPlayer.y - 32 / 2) - (arpiaPos.y - 32 / 2)) < (31)) 
		if ((glm::abs((posPlayer.x + 32 / 2) - (arpiaPos.x + 32 / 2)) < (31 / 2 + 32 / 2))) {
			PlaySound(TEXT("sounds/hit2.wav"), NULL, SND_ASYNC);
			return true;
		}
	return false;
}
void Player::update(int deltaTime, vector<Arpia*>& arpias, vector<Slug*>& slugs)
{
	sprite->update(deltaTime);
	spriteEspada->update(deltaTime);
	if (spriteEspada->getCurrentKeyFrame() == 3) {
		spriteEspada->changeAnimation(NONE);
	}
	if (is_behind(12*16+1) && map->getPortalApos().x != -1) {
		posPlayer.x = (map->getPortalApos().x+1) * 16;
		posPlayer.y= (map->getPortalApos().y-1) * 16;
	}
	if (is_behind(13*16+1) && map->getPortalNpos().x != -1) {
		posPlayer.x = (map->getPortalNpos().x+1) * 16;
		posPlayer.y = (map->getPortalNpos().y-1) * 16;
	}
	if (!shield) {
		for (int i = 0; i < arpias.size(); ++i) {
			if (colisionMonstruo(arpias[i])) {
				currentTime = clock();
				timeShield = TIME_SHIELD;
				shield = true;
				vida->setHP(vida->getHP() - 1);
			}
		}
		for (int i = 0; i < slugs.size(); ++i) {
			if (colisionMonstruo(slugs[i])) {
				currentTime = clock();
				timeShield = TIME_SHIELD;
				shield = true;
				vida->setHP(vida->getHP() - 1);
			}
		}
	}
	else {
		timeShield -= (clock() - currentTime);
		currentTime = clock();
		if (timeShield < 0) shield = false;
	}
	if (Game::instance().getKey('a'))
	{
		if (sprite->animation() != MOVE_LEFT)
			sprite->changeAnimation(MOVE_LEFT);
		posPlayer.x -= 2;
		if (map->collisionMoveLeft(posPlayer, glm::ivec2(32, 32)) || posPlayer.x<0)
		{
			posPlayer.x += 2;
			sprite->changeAnimation(STAND_LEFT);
		}
	}
	else if (Game::instance().getKey('d'))
	{
		if (sprite->animation() != MOVE_RIGHT)
			sprite->changeAnimation(MOVE_RIGHT);
		posPlayer.x += 2;
		if (map->collisionMoveRight(posPlayer, glm::ivec2(28, 45)) || posPlayer.x > 3970)
		{
			posPlayer.x -= 2;
			sprite->changeAnimation(STAND_RIGHT);
		}
	}
	else
	{
		if (sprite->animation() == MOVE_LEFT)
			sprite->changeAnimation(STAND_LEFT);
		else if (sprite->animation() == MOVE_RIGHT)
			sprite->changeAnimation(STAND_RIGHT);
	}

	if (bJumping)
	{
		jumpAngle += JUMP_ANGLE_STEP;
		if (jumpAngle == 180)
		{
			bJumping = false;
			posPlayer.y = startY;
		}
		else
		{
			int auxPlayerY = posPlayer.y;//pos antes de incrementar la altura (por si colisiono) 
			posPlayer.y = int(startY - 96 * sin(3.14159f * jumpAngle / 180.f));
			if (jumpAngle > 90)
				bJumping = !map->collisionMoveDown(posPlayer, glm::ivec2(28, 45), &posPlayer.y);
			else if (map->collisionMoveUp(posPlayer, glm::ivec2(28, 45), &posPlayer.y)) {
				jumpAngle = 180 - jumpAngle;
				posPlayer.y = auxPlayerY;
			}
		}
	}
	else
	{
		posPlayer.y += FALL_STEP;
		if (map->collisionMoveDown(posPlayer, glm::ivec2(28, 45), &posPlayer.y))
		{
			if (Game::instance().getKey('w'))
			{
				bJumping = true;
				jumpAngle = 0;
				startY = posPlayer.y;
			}
		}
	}
	if (posPlayer.y > 1330) {
		vida->setHP(0);
	}
	if (vida->getHP() == 0){
		posPlayer.x = spawnPoint.first;
		posPlayer.y = spawnPoint.second;
		vida->setHP(10);
	}
	std::pair<bool, bool> mouseStatus;
	if (!inventarioUI->isCrafteando()) {
		mouseStatus = Game::instance().getMouseStatus();
		Game::instance().setStatus(false, false);
		if (mouseStatus.first)// click izquierdo
		{
			timeActuar -= clock() - currentTimeAct;
			click(arpias, slugs);
			currentTimeAct = clock();
		}
	}
	else {
		mouseStatus = Game::instance().getMouseStatus();
		Game::instance().setStatus(false, false);
		if (mouseStatus.first)// click izquierdo
		{
			int x, y;
			Game::instance().getMousePositon(x, y);
			if (x > 7 && x < 51 && y > 425 && y < 470) {
				if (is_behind(160)) inventarioUI->craftearObjeto(160);
				else if (is_behind(146)) inventarioUI->craftearObjeto(144);
				else inventarioUI->craftearObjeto(0);
			}
		}
	}

	if (Game::instance().getKey('1')) {
		seleccionado = 0;
		inventarioUI->setSelectedItem(0);
	}
	else if (Game::instance().getKey('2')) {
		seleccionado = 1;
		inventarioUI->setSelectedItem(1);
	}
	else if (Game::instance().getKey('3')) {
		seleccionado = 2;
		inventarioUI->setSelectedItem(2);
	}
	else if (Game::instance().getKey('4')) {
		seleccionado = 3;
		inventarioUI->setSelectedItem(3);
	}
	else if (Game::instance().getKey('5')) {
		seleccionado = 4;
		inventarioUI->setSelectedItem(4);
	}
	else if (Game::instance().getKey('6')) {
		seleccionado = 5;
		inventarioUI->setSelectedItem(5);
	}
	else if (Game::instance().getKey('7')) {
		seleccionado = 6;
		inventarioUI->setSelectedItem(6);
	}
	else if (Game::instance().getKey('8')) {
		seleccionado = 7;
		inventarioUI->setSelectedItem(7);
	}
	else if (Game::instance().getKey('9')) {
		seleccionado = 8;
		inventarioUI->setSelectedItem(8);
	}
	else if (Game::instance().getKey('0')) {
		seleccionado = 9;
		inventarioUI->setSelectedItem(9);
	}
	else if (Game::instance().getKey('i')) {
	}
	else if (Game::instance().getKey('c')) {
		cPulsado = true;
	}
	if(cPulsado && !Game::instance().getKey('c')){
		inventarioUI->swapCrafteando();
		if(is_behind(160)) inventarioUI->possible_Craft(160);
		else if (is_behind(146) ) inventarioUI->possible_Craft(144);
		else inventarioUI->possible_Craft(0);
		cPulsado = false;
	}
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	spriteEspada->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x)-36.f, float(tileMapDispl.y + posPlayer.y)-31.f));
}

void Player::click(vector<Arpia*>& arpias, vector<Slug*>& slugs) {
	int x, y, rango,offset;
	offset = 0;
	rango = (5* 16);//tiles * tileSize;
	bool  is_inRange = false;
	Game::instance().getMousePositon(x, y);
	if (posPlayer.x < SCREEN_WIDTH / 2) {
		is_inRange = (abs(x - posPlayer.x) < rango);
		is_inRange = (abs(x - posPlayer.x) < rango);
		offset = -posPlayer.x + 512;
	}
	else if (posPlayer.x > 4000 - 1024 / 2) {
		is_inRange = (abs(x - (posPlayer.x - (4000 - 1024))) < rango);
		offset = -(posPlayer.x - (4000 - 1024)-512);	
	}
	else is_inRange = x < SCREEN_WIDTH / 2 + rango && x > SCREEN_WIDTH / 2 - rango && y < SCREEN_HEIGHT / 2 + rango && y > SCREEN_HEIGHT / 2 - rango;
	if (is_inRange && timeActuar<=0) {
		timeActuar = TIME_ACTUAR;
		//consumir pocion
		if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).is_consumible()&& Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getId()==22) {
			vida->setHP(vida->getHP() + 3);
			inventarioUI->modificarCantidad(seleccionado, -1);
			PlaySound(TEXT("sounds/drink.wav"), NULL, SND_ASYNC);
		}
		//Colocar objeto
		else if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).is_dropeable()) {
			if (map->colocarObjeto(x+offset, y, inventarioUI->getItemId(seleccionado))) {
				switch (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getTextureId())
				{
				case 1:
					PlaySound(TEXT("sounds/tierra.wav"), NULL, SND_ASYNC);
					break;
				case 2:
					PlaySound(TEXT("sounds/stone.wav"), NULL, SND_ASYNC);
					break;
				case 4:
					PlaySound(TEXT("sounds/wood.wav"), NULL, SND_ASYNC);
					break;
				case 9:
					PlaySound(TEXT("sounds/hielo.wav"), NULL, SND_ASYNC);
					break;
				case 5:
					//Hierro
					PlaySound(TEXT("sounds/metal.wav"), NULL, SND_ASYNC);
					break;
				case 6:
					//Cobre
					PlaySound(TEXT("sounds/metal.wav"), NULL, SND_ASYNC);
					break;
				case 12:
					//Cama
					spawnPoint.first = (((x + Game::instance().scene.getDespCamX()) / 16) + SCREEN_WIDTH / 16 - 96)*16;
					spawnPoint.second = ((((y + Game::instance().scene.getDespCamY()) / 16) - SCREEN_HEIGHT / 16 + 24) - 2)*16;
					break;
				}
				inventarioUI->modificarCantidad(seleccionado, -1);
			}
		}
		//Minar objeto
		else if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).is_mineria()) {
			if(posPlayer.x>4000-1024/2){
				cout << posPlayer.x << "  " << x << endl;
				if (x < posPlayer.x- (4000 - 1024)) {
					spriteEspada->changeAnimation(MIN_LEFT);
					sprite->changeAnimation(STAND_LEFT);
				}
				else {
					spriteEspada->changeAnimation(MIN_RIGHT);
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
			else if (posPlayer.x < 1024 / 2) {
				cout << posPlayer.x << "  " << x << endl;
				if (x < posPlayer.x) {
					spriteEspada->changeAnimation(MIN_LEFT);
					sprite->changeAnimation(STAND_LEFT);
				}
				else {
					spriteEspada->changeAnimation(MIN_RIGHT);
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
			else {
				if (x < SCREEN_WIDTH / 2) {
					spriteEspada->changeAnimation(MIN_LEFT);
					sprite->changeAnimation(STAND_LEFT);
				}
				else {
					spriteEspada->changeAnimation(MIN_RIGHT);
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
			objeto extraido = map->extraer(x, y, Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)));
			if (extraido.getId() != -1) {
				int objID = extraido.getId();
				if (extraido.getTextureId() == 4) objID = 3;
				anadirObjeto(objID, 1);
				switch (extraido.getTextureId())
				{
				case 1:
					PlaySound(TEXT("sounds/tierra.wav"), NULL, SND_ASYNC);
					break;	
				case 2:
					PlaySound(TEXT("sounds/stone.wav"), NULL, SND_ASYNC);
					break;
				case 4:
					PlaySound(TEXT("sounds/wood.wav"), NULL, SND_ASYNC);
					break;
				case 9:
					PlaySound(TEXT("sounds/hielo.wav"), NULL, SND_ASYNC);
					break;
				case 5:
					//Hierro
					PlaySound(TEXT("sounds/metal.wav"), NULL, SND_ASYNC);
					break;
				case 6:
					//Cobre
					PlaySound(TEXT("sounds/metal.wav"), NULL, SND_ASYNC);
					break;

				default:
					break;
				}
			}

		}
		else if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getAtaque() > 0) {
			PlaySound(TEXT("sounds/espada_aire.wav"), NULL, SND_ASYNC);
			if ((x < SCREEN_WIDTH / 2 && posPlayer.x> 1024 / 2 && posPlayer.x<4000-1024/2) || (posPlayer.x < 1024 / 2 && x < posPlayer.x) || (posPlayer.x>4000 - 1024 / 2 && x<(posPlayer.x-(4000-1024)))) {
				if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getTextureId() == 3) {
					spriteEspada->changeAnimation(ATTACK_SWORD1_RIGHT);
					sprite->changeAnimation(STAND_LEFT);
				}
				else if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getTextureId() == 13) {
					spriteEspada->changeAnimation(ATTACK_SWORD2_RIGHT);
					sprite->changeAnimation(STAND_LEFT);
				}
				for (int i = 0; i < arpias.size(); ++i) {
					if (glm::abs(arpias[i]->getPosition().x + 92 / 2 - (posPlayer.x + 28 / 2)) < 52 + 92 / 2)
						if (glm::abs(arpias[i]->getPosition().y + 67 / 2 - posPlayer.y) < 52 + 67 / 2) {
							arpias[i]->impacto(Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getAtaque());
							if (arpias[i]->getVida() < 0) {
								arpias.erase(arpias.begin() + i);
								inventarioUI->setObjectToInventoryPos(21);
							}
							PlaySound(TEXT("sounds/hit.wav"), NULL, SND_ASYNC);
						}
				}
				for (int i = 0; i < slugs.size(); ++i) {
					int offset = 0;
					if (slugs[i]->getPosition().x < posPlayer.x) offset = 50;
					if (glm::abs(slugs[i]->getPosition().x + 34 / 2 - (posPlayer.x + 28 / 2)) < 52 + 45)
						if (glm::abs(slugs[i]->getPosition().y + 23 / 2 - (posPlayer.y + 45 / 2)) < 30 + 20) {
							slugs[i]->impacto(Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getAtaque());
							if (slugs[i]->getVida() < 0) {
								slugs.erase(slugs.begin() + i);
								inventarioUI->setObjectToInventoryPos(20);
							}
							PlaySound(TEXT("sounds/hit.wav"), NULL, SND_ASYNC);
						}
				}
			}
			else {
				if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getTextureId() == 3) {
					spriteEspada->changeAnimation(ATTACK_SWORD1_LEFT);
					sprite->changeAnimation(STAND_RIGHT);
				}
				else if (Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getTextureId() == 13) {
					spriteEspada->changeAnimation(ATTACK_SWORD2_LEFT);
					sprite->changeAnimation(STAND_RIGHT);
				}
				for (int i = 0; i < arpias.size(); ++i) {
					if (glm::abs(arpias[i]->getPosition().x + 92 / 2 - posPlayer.x + 28 / 2) < 52 + 92 / 2)
						if (glm::abs(arpias[i]->getPosition().y + 67 / 2 - posPlayer.y) < 52 + 67 / 2) {
							arpias[i]->impacto(Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getAtaque());
							if (arpias[i]->getVida() < 0) {
								arpias.erase(arpias.begin() + i);
								inventarioUI->setObjectToInventoryPos(21);
							}
							PlaySound(TEXT("sounds/hit.wav"), NULL, SND_ASYNC);
						}
				}
				for (int i = 0; i < slugs.size(); ++i) {
					if (glm::abs(slugs[i]->getPosition().x + 34 / 2 - posPlayer.x + 28 / 2) < 52 + 45)
						if (glm::abs(slugs[i]->getPosition().y + 23 / 2 - posPlayer.y + 45 / 2) < 52 + 23) {
							slugs[i]->impacto(Cjt_Objetos::instance().getObjeto(inventarioUI->getItemId(seleccionado)).getAtaque());
							if (slugs[i]->getVida() <= 0) {
								slugs.erase(slugs.begin() + i);
								inventarioUI->setObjectToInventoryPos(20);
							}
							PlaySound(TEXT("sounds/hit.wav"), NULL, SND_ASYNC);
						}
				}
			}
		}	
	}
	if (x > 0 && x < 447 && y > 0 && y < 93) {
		//Click en el inventario;
		int inventoryX = 0;
		inventoryX = x / 46;
		if (y < 42) {
			seleccionado = inventoryX;
			inventarioUI->setSelectedItem(inventoryX);
		}
		else {
			inventarioUI->swapItems(10 + inventoryX, seleccionado);
		}
	}
}

void Player::render()
{
	if (shield == true && timeShield%100<20) {
	}
	else sprite->render();
	spriteEspada->render();
}

bool Player::is_behind(int id) {
	return map->getPos(posPlayer)==id;
}

void Player::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Player::setInventory(InventarioUI * inventario)
{
	inventarioUI = inventario;
}

void Player::setHPUI(HPUI * HPUI)
{
	vida = HPUI;
}

void Player::setPosition(const glm::vec2 &pos)
{
	posPlayer = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
}
glm::vec2 Player::getPosition()
{
	return posPlayer;
}

void Player::anadirObjeto(int id, int num) {
	inventarioUI->setObjectToInventoryPos(id);
}




