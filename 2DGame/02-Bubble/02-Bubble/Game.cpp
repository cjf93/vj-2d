#include <GL/glew.h>
#include <GL/glut.h>
#include <thread> 
#include "Game.h"




void Game::init()
{
	bPlay = true;
	playGame = false;
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	//first.join();
	menuscene.init();
	scene.init();
}

bool Game::update(int deltaTime)
{
	if (menuscene.getGoToGame() == true) {
		menuscene.setGoToGame(false);
		menuscene.setcurrentScreen(0);		
		playGame = true;
		scene.set_Paused(false);
	}
	if(!playGame) 	menuscene.update(deltaTime);
	else scene.update(deltaTime);
	if( menuscene.getExit()) 
		exit(0);
	if (scene.is_Paused()) {
		playGame = false;
	}
	return bPlay;
}

void Game::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(!playGame)menuscene.render();
	else scene.render();
}

void Game::keyPressed(int key)
{
	/*if(key == 27) // Escape code
		bPlay = false;*/
	
	keys[key] = true;
}

void Game::keyReleased(int key)
{
	keys[key] = false;
}

void Game::specialKeyPressed(int key)
{
	specialKeys[key] = true;
}

void Game::specialKeyReleased(int key)
{
	specialKeys[key] = false;
}

void Game::mouseMove(int x, int y)
{
}

void Game::mousePress(int button,int x, int y)
{

	if (button == 0) {
		mouseStatus.first=true; // se ha clicado
		mousePosX = x;
		mousePosY = y;	
	}

}

void Game::mouseRelease(int button)
{
	if (button == 0) {
		mouseStatus.first = false;
	}
}

bool Game::getKey(int key) const
{
	return keys[key];
}

bool Game::getSpecialKey(int key) const
{
	return specialKeys[key];
}

std::pair<bool,bool> Game::getMouseStatus()
{
	return mouseStatus;
}
void Game::setStatus(bool a, bool b)
{
	mouseStatus = std::pair<bool, bool>(a, b);
}
void Game::getMousePositon(int& x, int& y) {
	x = mousePosX;
	y = mousePosY;
}







