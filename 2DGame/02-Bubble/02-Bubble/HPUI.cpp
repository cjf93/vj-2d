#include "HPUI.h"


void HPUI::init(const glm::ivec2 & tileMapPos, const glm::ivec2 & mapSize, ShaderProgram & shaderProgram)
{
	this->shaderProgram = shaderProgram;
	HPTextures = vector<Sprite*>(10);
	vida = vector<int>(10, 1);
	xOffset = (mapSize.x / 2) - 30;
	yOffset = (mapSize.y / 2) - 5;
	tileMapDispl = tileMapPos;
	for (int i = 0; i < 10; ++i) {
		HPTextures[i] = createVida(vida[i], shaderProgram);
	}
}

Sprite * HPUI::createVida(int objectId, ShaderProgram & shaderProgram)
{
	Sprite *sprite;
	HPTexture.loadFromFile("images/HPTilesheet.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(22, 22), glm::vec2(1, 0.5f), &HPTexture, &shaderProgram);
	sprite->setNumberAnimations(1);

	float f = objectId *0.5f;
	sprite->setAnimationSpeed(0, 1);
	sprite->addKeyframe(0, glm::vec2(0.f, f));
	sprite->changeAnimation(0);
	return sprite;
}

void HPUI::update(int deltaTime, glm::vec2 posPlayer)
{
	if (posPlayer.x > 4000 - 1024 / 2) posPlayer.x = 4000 - 1024 / 2;
	if (posPlayer.x < 1024 / 2) posPlayer.x = 1024 / 2;
	for (int i = 0; i < 10; ++i) {
		HPTextures[i]->setPosition(glm::vec2(float(posPlayer.x+ xOffset), float(posPlayer.y - yOffset +i*25)));
	}
}

void HPUI::render()
{
	for each (Sprite *s in HPTextures)
	{
		if (s != NULL) s->render();
	}
}

void HPUI::setPosition(const glm::vec2 & pos)
{
	posHPUI = pos;
}

glm::vec2 HPUI::getPosition()
{
	return posHPUI;
}

void HPUI::setHP(int HP)
{
	if (HP > 10) HP = 10;
	if (HP < 0) {
		return;
	}
	else {
		for (int i = 0; i < HP; ++i) {
			if (vida[i] != 1) {
				delete HPTextures[i];
				HPTextures[i] = createVida(1, shaderProgram);
			}
			vida[i] = 1;
		}
		for (int i = HP; i < 10; ++i) {
			if (vida[i] != 0) {
				delete HPTextures[i];
				HPTextures[i] = createVida(0, shaderProgram);
			}
			vida[i] = 0;
		}
	}
}

int HPUI::getSize()
{
	return vida.size();
}

int HPUI::getHP()
{
	int hitPoints = 0;
	for (int i = 0; i < 10; ++i) {
		if (vida[i] == 1) ++hitPoints;
	};
	return hitPoints;
}
