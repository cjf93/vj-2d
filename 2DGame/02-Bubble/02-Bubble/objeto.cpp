#include "objeto.h"
using namespace std;



objeto::objeto()
{
	this->id = -1;
	this->textureId = -1;
	this->usable = 0;
	this->crafteable = 0;
	this->dureza = 0;
	this->ataque = 0;
	this->dropeable = 0;
	this->mineria = 0;
	this->objetonecesario = 0;
}

objeto::objeto(int id, int textureId, bool usable, bool consumible, bool crafteable, int dureza, int ataque, bool dropeable, bool mineria, int objetonecesario, vector<pair<int, int>> dependencias) {
	this->id = id;
	this->textureId = textureId;
	this->usable = usable;
	this->consumible = consumible;
	this->crafteable = crafteable;
	this->dureza = dureza;
	this->ataque = ataque;
	this->dropeable = dropeable;
	this->mineria = mineria;
	this->dependencias = dependencias;
	this->objetonecesario = objetonecesario;
}

int objeto::getId() {
	return id;
}
int objeto::getTextureId() {
	return textureId;
}
bool objeto::is_usable(){
	return usable;
}
bool objeto::is_consumible() {
	return consumible;
}
bool objeto::is_crafteable() {
	return crafteable;
}
int objeto::getDureza() {
	return dureza;
}
int objeto::getAtaque() {
	return ataque;
}
bool objeto::is_dropeable() {
	return dropeable;
}

bool objeto::is_mineria() {
	return mineria;
}

int objeto::getObjetoNecesario()
{
	return objetonecesario;
}

vector<pair<int, int>> objeto::getDependencias()
{
	return  dependencias;
}

objeto::~objeto()
{
}
