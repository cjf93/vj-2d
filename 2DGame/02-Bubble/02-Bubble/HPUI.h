#ifndef _HPUI_INCLUDE
#define _HPUI_INCLUDE


#include "Sprite.h"
#include "TileMap.h"
#include "objeto.h"
#include "Cjt_Objetos.h"


class HPUI
{

public:
	void init(const glm::ivec2 &tileMapPos, const glm::ivec2 &mapSize, ShaderProgram & shaderProgram);
	void update(int deltaTime, glm::vec2 posPlayer);
	void render();
	void setPosition(const glm::vec2 &pos);
	glm::vec2 getPosition();
	void setHP(int HP);
	int getSize();
	int getHP();

private:
	Sprite* createVida(int vida, ShaderProgram &shaderProgram);
private:
	ShaderProgram shaderProgram;
	Texture HPTexture;
	vector<Sprite*> HPTextures;
	glm::ivec2 tileMapDispl, posHPUI;
	float xOffset, yOffset;
	vector<int> vida; //1 = corazon 0 | = no
};


#endif // _HPUI_INCLUDE




