#ifndef _GAME_INCLUDE
#define _GAME_INCLUDE


#include "GameScene.h"
#include "MenuScene.h"

#define Password uservj25 vunnIsp6

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768


// Game is a singleton (a class with a single instance) that represents our whole application


class Game
{

public:
	Game() {}
	
	
	static Game &instance()
	{
		static Game G;
	
		return G;
	}
	
	void init();
	bool update(int deltaTime);
	void render();
	
	// Input callback methods
	void keyPressed(int key);
	void keyReleased(int key);
	void specialKeyPressed(int key);
	void specialKeyReleased(int key);
	void mouseMove(int x, int y);
	void mousePress(int button,int x, int y);
	void mouseRelease(int button);
	
	bool getKey(int key) const;
	bool getSpecialKey(int key) const;
	std::pair<bool,bool> getMouseStatus();
	void setStatus(bool a, bool b);

	void getMousePositon(int & x, int & y);
	GameScene scene;                      // Scene to render
	MenuScene menuscene;

private:
	bool bPlay;                       // Continue to play game?
	bool playGame;
	bool keys[256], specialKeys[256]; // Store key states so that  we can have access at any time
	std::pair<bool,bool> mouseStatus; //clic activado 
	int mousePosX ;
	int mousePosY ;

};


#endif // _GAME_INCLUDE


