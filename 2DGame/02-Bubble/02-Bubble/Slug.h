#pragma once
#include "Sprite.h"
#include "TileMap.h"
#include "objeto.h"

class Slug
{
public:
	Slug();
	~Slug();
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime, glm::ivec2 playerPos);
	void render();
	void setTileMap(TileMap *tileMap);
	void impacto(int hit);
	void setPosition(const glm::vec2 &pos);
	glm::vec2 getPosition();
	int getVida();

private:
	int tomarDecision(glm::ivec2 posPLayer);
	bool bJumping;
	int vida=3;
	glm::ivec2 tileMapDispl, posSlug;
	int jumpAngle, startY;
	Texture spritesheet;
	Sprite *sprite;
	int speed;
	TileMap *map;
};

