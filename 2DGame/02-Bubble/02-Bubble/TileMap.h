#ifndef _TILE_MAP_INCLUDE
#define _TILE_MAP_INCLUDE


#include <glm/glm.hpp>
#include "Texture.h"
#include "ShaderProgram.h"
#include <vector>
#include "objeto.h"

// Class Tilemap is capable of loading a tile map from a text file in a very
// simple format (see level01.txt for an example). With this information
// it builds a single VBO that contains all tiles. As a result the render
// method draws the whole map independently of what is visible.

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768
#define LINIANOCOLLIDE 8


class TileMap
{

public:
	// Tile maps can only be created inside an OpenGL context
	static TileMap *createTileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program);

	TileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program);
	~TileMap();

	void render() const;
	void free();
	
	int getTileSize() const { return tileSize; }
	int getPos(glm::ivec2 pos);
	bool colocarObjeto(int posX, int posY, int objId);
	objeto extraer(int posX, int posY,objeto obj);

	bool is_empty(int posX, int posY);
	int alturaSuelo(glm::ivec2 posArpia);
	glm::ivec2 getPortalApos();
	glm::ivec2 getPortalNpos();
	bool collisionMoveLeft(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveRight(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveUp(const glm::ivec2 & pos, const glm::ivec2 & size, int * posY) const;
	bool collisionMoveDown(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY) const;
    void prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program);
	void updateArrays(const glm::vec2 &minCoords, glm::vec2 renderTile, ShaderProgram &program, int idObj, int offset);
	
private:
	bool loadLevel(const string &levelFile);
	

	vector<string> Split(string s, char split, int size);

	int getIntValue(string s);

private:
	GLuint vao;
	GLuint vbo;
	GLint posLocation, texCoordLocation;
	vector<float> vertices;
	glm::ivec2 position, mapSize, tilesheetSize;
	int tileSize, blockSize;
	Texture tilesheet;
	glm::vec2 tileTexSize;
	int *map;
	glm::vec2 MyminCoords;
	ShaderProgram Myprogram;
	int nTiles;
	glm::ivec2 portalNPos= glm::ivec2 (-1,-1);
	glm::ivec2 portalAPos= glm::ivec2(-1, -1);

};


#endif // _TILE_MAP_INCLUDE


