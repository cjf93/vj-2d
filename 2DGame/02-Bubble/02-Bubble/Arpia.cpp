#include "Arpia.h"



Arpia::Arpia()
{
}


Arpia::~Arpia()
{
}

#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Arpia.h"
#include "Game.h"
#include "reader.h"
#include <stdlib.h>


#define JUMP_ANGLE_STEP 4
#define JUMP_HEIGHT 96
#define FALL_STEP 4
#define TAMANO_INVENTARIO 16
#define ALTURA_VUELO 20


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT
};
enum ArpiaDecisiones
{
	GO_LEFT, GO_RIGHT, GO_ATACK, GO_HIGH, STAND
};


void Arpia::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	bAtacking = false;
	bgoingUp=true;
	spritesheet.loadFromFile("images/ArpiaTileSheet.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(92, 67), glm::vec2(0.5, 0.5), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(4);

	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.5f, 0.0f));
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.5f, 0.5f));

	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.5f, 0.0f));
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.5f, 0.5f));

	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.5f, 0.0f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.5f, 0.5f));

	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 0.5f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5f, 0.0f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.5f, 0.5f));

	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	speed = 1;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posArpia.x), float(tileMapDispl.y + posArpia.y)));


}

int Arpia::tomarDecision(glm::ivec2 posPLayer) {
	int decision = STAND;
	glm::ivec2 auxPosArpia = posArpia;
	auxPosArpia.x -= speed;
	if (posPLayer.x < posArpia.x && !map->collisionMoveLeft(posArpia, glm::ivec2(32, 32))) decision = GO_LEFT;
	else if (posPLayer.x > posArpia.x  &&  !map->collisionMoveRight(posArpia, glm::ivec2(32, 32))) decision = GO_RIGHT;
	else decision = GO_ATACK;
	return decision;
}

void Arpia::update(int deltaTime, glm::ivec2 playerPos)
{
	sprite->update(deltaTime);
	int decision = STAND;
	if (glm::abs((playerPos.x + 32 / 2) - (posArpia.x + 32 / 2)) < 15 * 16) {
		if ((glm::abs((playerPos.y + 32 / 2) - (posArpia.y + 32 / 2)) < 40 * 16)) {
			int decision = tomarDecision(playerPos);
			if (decision == GO_LEFT)
			{
				if (sprite->animation() != MOVE_LEFT)
					sprite->changeAnimation(MOVE_LEFT);
				posArpia.x -= speed;
				if (map->collisionMoveLeft(posArpia, glm::ivec2(32, 32)))
				{
					posArpia.x += speed;
					sprite->changeAnimation(STAND_LEFT);
				}
			}
			else if (decision == GO_RIGHT)
			{
				if (sprite->animation() != MOVE_RIGHT)
					sprite->changeAnimation(MOVE_RIGHT);
				posArpia.x += speed;
				if (map->collisionMoveRight(posArpia, glm::ivec2(28, 45)))
				{
					posArpia.x -= speed;
					sprite->changeAnimation(STAND_RIGHT);
				}
			}
			else
			{
				if (sprite->animation() == MOVE_LEFT)
					sprite->changeAnimation(STAND_LEFT);
				else if (sprite->animation() == MOVE_RIGHT)
					sprite->changeAnimation(STAND_RIGHT);
			}
			if (GO_ATACK && !bgoingUp) bAtacking = true;
			if (bAtacking)
			{
				if (posArpia.y >= playerPos.y)
				{
					bAtacking = false;
					bgoingUp = true;
					height = map->alturaSuelo(posArpia) - ALTURA_VUELO;
				}
				else
				{
					int auxArpiaY = posArpia.y;//pos antes de incrementar la altura (por si colisiono) 
					posArpia.y += speed * 2;
					if (map->collisionMoveDown(posArpia, glm::ivec2(28, 45), &posArpia.y)) {
						posArpia.y -= speed * 2;
						bgoingUp = true;
						bAtacking = false;
					}
				}
			}
			if (bgoingUp && (posArpia.y < height * 16 || map->collisionMoveUp(posArpia, glm::ivec2(28, 45), &posArpia.y - speed * 2))) bgoingUp = false;
			else if (bgoingUp) {
				int auxArpiaY = posArpia.y;//pos antes de incrementar la altura (por si colisiono) 
				posArpia.y -= speed * 2;
				if (map->collisionMoveUp(posArpia, glm::ivec2(28, 45), &posArpia.y)) {
					posArpia.y = auxArpiaY;
				}
			}
		}
	}
	else {
		bAtacking = true;
		bgoingUp = false;
	}
	std::pair<bool, bool> mouseStatus;
	mouseStatus = Game::instance().getMouseStatus();
	Game::instance().setStatus(false, false);

	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posArpia.x), float(tileMapDispl.y + posArpia.y)));
}



void Arpia::render()
{
	sprite->render();
}

void Arpia::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Arpia::impacto(int hit)
{
	vida = vida - hit;
}

void Arpia::setPosition(const glm::vec2 &pos)
{
	posArpia = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posArpia.x), float(tileMapDispl.y + posArpia.y)));
}
glm::vec2 Arpia::getPosition()
{
	return posArpia;
}

int Arpia::getVida()
{
	return vida;
}


