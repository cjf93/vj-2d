/*#include "al.h" 
#include "alc.h" 
//#include "AL/alut.h"
#include "EFX-Util.h"
#include "efx-creative.h"
#include "efx.h"
#include <hash_map>
using namespace std;
#define NUM_BUFFERS 15
#define NUM_SOURCES 15


class cSound
{
    public:
	  cSound(void);
	  ~cSound(void);
      void init();
      void play(ALbyte *source);
      void pause(ALbyte *source);
	  void finish();
	  void initSound(ALbyte *source);
	  void initSong(ALbyte *source);
	  void modify_song_gain(ALbyte *source, float value);
	  void modify_sound_gain(ALbyte *source, float value);
	  ALboolean CreateAuxEffectSlot(ALuint *puiAuxEffectSlot);
      ALboolean CreateEffect(ALuint *puiEffect, ALenum eEffectType);
      ALboolean SetEFXEAXReverbProperties(EFXEAXREVERBPROPERTIES *pEFXEAXReverb, ALuint uiEffect);

private:
	ALuint source[NUM_SOURCES];
	ALuint buffers[NUM_BUFFERS];
	ALCenum error;
	ALenum     format;
	ALsizei    size;
	ALsizei    freq;
	ALboolean  loop;
	ALvoid*    data;
	hash_map<ALbyte*, int> sourcelist;
	int pos;
	ALuint		EffectSlot, Effect;
	ALboolean	bEffectCreated;
	EFXEAXREVERBPROPERTIES efxReverb;
	LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti;
};*/