#include <iostream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "MenuScene.h"
#include "Game.h"


#define SCREEN_X 0
#define SCREEN_Y 0


MenuScene::MenuScene()
{
}

MenuScene::~MenuScene()
{
}


void MenuScene::init()
{
	initShaders();
	Screens = vector<Sprite*>(3);
	currentScreen = 0;
	exit = false;
	goToGame = false;
	projection = glm::ortho(0.f, float(SCREEN_WIDTH - 1), float(SCREEN_HEIGHT - 1), 0.f);
	currentTime = 0.0f;
	//Menu
	Texture1.loadFromFile("images/PantallaMenu.png", TEXTURE_PIXEL_FORMAT_RGBA);
	Screens[0] = Sprite::createSprite(glm::ivec2(1024, 768), glm::vec2(1, 1), &Texture1, &texProgram);
	Screens[0]->setNumberAnimations(1);
	Screens[0]->setAnimationSpeed(0, 1);
	Screens[0]->addKeyframe(0, glm::vec2(0.f, 0.f));
	Screens[0]->changeAnimation(0);
	Screens[0]->setPosition(glm::vec2(float(0), float(0)));

	//Credits
	Texture2.loadFromFile("images/PantallaCredits.png", TEXTURE_PIXEL_FORMAT_RGBA);
	Screens[1] = Sprite::createSprite(glm::ivec2(1024, 768), glm::vec2(1, 1), &Texture2, &texProgram);
	Screens[1]->setNumberAnimations(1);
	Screens[1]->setAnimationSpeed(0, 1);
	Screens[1]->addKeyframe(0, glm::vec2(0.f, 0.f));
	Screens[1]->changeAnimation(0);
	Screens[1]->setPosition(glm::vec2(float(0), float(0)));

	//Instructions
	Texture3.loadFromFile("images/PantallaInstructions.png", TEXTURE_PIXEL_FORMAT_RGBA);
	Screens[2] = Sprite::createSprite(glm::ivec2(1024, 768), glm::vec2(1, 1), &Texture3, &texProgram);
	Screens[2]->setNumberAnimations(1);
	Screens[2]->setAnimationSpeed(0, 1);
	Screens[2]->addKeyframe(0, glm::vec2(0.f, 0.f));
	Screens[2]->changeAnimation(0);
	Screens[2]->setPosition(glm::vec2(float(0), float(0)));
}

void MenuScene::update(int deltaTime)
{
	currentTime += deltaTime; 
	int x, y;
	std::pair<bool, bool> mouseStatus;
	mouseStatus = Game::instance().getMouseStatus();
	Game::instance().setStatus(false, false);
	if (mouseStatus.first)// click izquierdo
	{
		click();
	}
}

void MenuScene::click()
{
	int x, y;
	Game::instance().getMousePositon(x, y);
	if (currentScreen == 0) {
		//Estoy en las X de los botones
		if (x > 130 && x < 893) {
			//Boton Play
			if (y > 311 && y < 383) {
				goToGame = true;
			}
			//Botton Instrucciones
			else if (y > 396 && y < 470) {
				currentScreen = 2;
			}
			//Botton Credits
			else if (y > 485 && y < 552) {
				currentScreen = 1;
			}
			//Botton Exit
			else if (y > 571 && y < 645) {
				exit = true;
			}
		}
	}
	else {
		if (x > 45 && x < 236 && y > 631 && y < 707) {
			//Boton Exit
			currentScreen = 0;
		}
	}
	
}

void MenuScene::render()
{
	glm::mat4 modelview;

	texProgram.use();
	texProgram.setUniformMatrix4f("projection", projection);
	texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
	modelview = glm::mat4(1.0f);
	texProgram.setUniformMatrix4f("modelview", modelview);
	texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);

	Screens[currentScreen]->render();
}

bool MenuScene::getExit()
{
	return exit;
}

bool MenuScene::getGoToGame()
{
	return goToGame;
}

void MenuScene::setGoToGame(bool b)
{
	goToGame = b;
}

void MenuScene::setcurrentScreen(int s)
{
	currentScreen = s;
}

void MenuScene::initShaders()
{
	Shader vShader, fShader;

	vShader.initFromFile(VERTEX_SHADER, "shaders/texture.vert");
	if (!vShader.isCompiled())
	{
		cout << "Vertex Shader Error" << endl;
		cout << "" << vShader.log() << endl << endl;
	}
	fShader.initFromFile(FRAGMENT_SHADER, "shaders/texture.frag");
	if (!fShader.isCompiled())
	{
		cout << "Fragment Shader Error" << endl;
		cout << "" << fShader.log() << endl << endl;
	}
	texProgram.init();
	texProgram.addShader(vShader);
	texProgram.addShader(fShader);
	texProgram.link();
	if (!texProgram.isLinked())
	{
		cout << "Shader Linking Error" << endl;
		cout << "" << texProgram.log() << endl << endl;
	}
	texProgram.bindFragmentOutput("outColor");
	vShader.free();
	fShader.free();
}



