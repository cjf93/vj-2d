#include "Minimapa.h"
#include <GL/glew.h>
#include <GL/glut.h>

void Minimapa::init(const glm::ivec2 &tileMapPos, const glm::ivec2 &mapSize, ShaderProgram &shaderProgram)
{
	this->shaderProgram = shaderProgram;
	xOffset = -500;
	yOffset = 500;
	tileMapDispl = tileMapPos;
	minimapa_sprite = createMinimapa(shaderProgram);
	cara_sprite = createCara(shaderProgram);
}

void Minimapa::setPintar(bool pintar) {
	this->pintar = pintar;
}
void Minimapa::update(int deltaTime, glm::vec2 posPlayer)
{

		minimapa_sprite->setPosition(glm::vec2(float(posPlayer.x + xOffset), float(posPlayer.y - yOffset + 9*25)));
		cara_sprite->setPosition(glm::vec2(float(posPlayer.x + xOffset+(posPlayer.x*1001.0/(250.0*16))), float(posPlayer.y-yOffset+ (posPlayer.y*403.0 / (100.0 * 16)) + 9 * 25)));
}

void Minimapa::render()
{
	minimapa_sprite->render();
	cara_sprite->render();
}

void Minimapa::setPosition(const glm::vec2 & pos)
{
	posMinimapa = pos;
}

glm::vec2 Minimapa::getPosition()
{
	return posMinimapa;
}

Sprite * Minimapa::createMinimapa(ShaderProgram & shaderProgram)
{
	Sprite *sprite;
	miniMapa_texture.loadFromFile("images/minimapa.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(1001, 403), glm::vec2(1, 1), &miniMapa_texture, &shaderProgram);
	sprite->setNumberAnimations(1);
	sprite->setAnimationSpeed(0, 1);
	sprite->addKeyframe(0, glm::vec2(0.f, 0.f));
	sprite->changeAnimation(0);
	return sprite;
}

Sprite * Minimapa::createCara(ShaderProgram & shaderProgram)
{
	Sprite *sprite;
	cara_texture.loadFromFile("images/cara.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(27, 25), glm::vec2(1, 1), &cara_texture, &shaderProgram);
	sprite->setNumberAnimations(1);
	sprite->setAnimationSpeed(0, 1);
	sprite->addKeyframe(0, glm::vec2(0.f, 0.f));
	sprite->changeAnimation(0);
	return sprite;
}
