#ifndef _INVENTARIOUI_INCLUDE
#define _INVENTARIOUI_INCLUDE


#include "Sprite.h"
#include "TileMap.h"
#include "objeto.h"
#include "Cjt_Objetos.h"


class InventarioUI
{

public:
	void init(const glm::ivec2 &tileMapPos, const glm::ivec2 &screenSize, ShaderProgram & shaderProgram);
	void update(int deltaTime ,glm::vec2 posPlayer);
	void render();
	void setPosition(const glm::vec2 &pos);
	glm::vec2 getPosition();
	void setObjectToInventoryPos(int objectId);
	void setSelectedItem(int inventorySpace);
	int getItemId(int x);
	void InventarioUI::possible_Craft(int id);
	bool isFull();
	bool isCrafteando();
	int getSize();
	int getCantidad(int x);
	void craftearObjeto(int id);
	void modificarCantidad(int posicion, int cantidad);// sumas la variable cantidad
	void setCantidad(int posicion, int cantidad); //haces set de la cantidad
	void swapItems(int firstItemId, int secondItemId);
	void swapCrafteando();
private:
	Sprite* createObject(int objectId, ShaderProgram &shaderProgram);
	Sprite* createNumber(int number, ShaderProgram &shaderProgram);
private:
	ShaderProgram shaderProgram;
	Texture UITexture;
	Texture objectTexture;
	Texture slectedTexture;
	Texture CraftingTexture;
	Texture NumbersTexture;

	Sprite *selectedSprite;
	Sprite *sprite;
	Sprite *craftingMenu;
	vector<Sprite*> inventoryObjects;
	vector<Sprite*> craftingSprites;
	vector<objeto> possibleCrafting;
	vector < pair<Sprite*, Sprite*>> cantidadesInventario;
	vector<pair<Sprite*, pair<Sprite*, Sprite*>>> cantidadesCrafting;
	glm::ivec2 tileMapDispl, posInventario;
	float xOffset, yOffset;
	vector<pair<int, int>> inventario; //tipo y cantidad
	int seleccionado;
	int craftingMostrando;
	bool crafteando;
	bool upPulsado;
	bool downPulsado;
};


#endif // _INVENTARIOUI_INCLUDE




