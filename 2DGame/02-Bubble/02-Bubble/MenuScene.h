#ifndef _MENUSCENE_INCLUDE
#define _MENUSCENE_INCLUDE


#include <glm/glm.hpp>
#include "ShaderProgram.h"
#include "Texture.h"
#include "Sprite.h"


// Scene contains all the entities of our game.
// It is responsible for updating and render them.


class MenuScene
{

public:
	MenuScene();
	~MenuScene();

	void init();
	void update(int deltaTime);
	void click();
	void render();
	bool getExit();
	bool getGoToGame();
	void setGoToGame(bool b);
	void setcurrentScreen(int s);

private:
	void initShaders();

private:
	vector<Sprite*> Screens;
	Texture Texture1; //Screen 1
	Texture Texture2; //Screen 2
	Texture Texture3; //Screen 3
	int currentScreen;
	ShaderProgram texProgram;
	float currentTime;
	glm::mat4 projection;
	bool exit;
	bool goToGame;
};


#endif // _SCENE_INCLUDE

