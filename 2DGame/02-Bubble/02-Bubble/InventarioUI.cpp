#include "InventarioUI.h"
#include "Game.h"
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>


void InventarioUI::init(const glm::ivec2 &tileMapPos, const glm::ivec2 &mapSize, ShaderProgram &shaderProgram)
{
	this->shaderProgram = shaderProgram;
	inventoryObjects = vector<Sprite*>(20);
	craftingSprites = vector<Sprite*>(3);
	cantidadesInventario = vector<pair<Sprite*, Sprite*>>(20);
	pair<int, int> auxPair(-1, 0);
	upPulsado = false;
	downPulsado = false;
	inventario = vector<pair<int, int>>(20,auxPair);
	xOffset = mapSize.x/2;
	yOffset = mapSize.y/2;
	crafteando = false;
	craftingMostrando = 0;
	UITexture.loadFromFile("images/Inventario.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(465, 98), glm::vec2(1, 1), &UITexture, &shaderProgram);
	sprite->setNumberAnimations(1);


	sprite->setAnimationSpeed(0, 1);
	sprite->addKeyframe(0, glm::vec2(0.f, 0.f));
	sprite->changeAnimation(0);
	sprite->setPosition(glm::vec2(0, 0));
	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(xOffset), float(yOffset)));
	seleccionado = 0;

	slectedTexture.loadFromFile("images/inventario_selected.png", TEXTURE_PIXEL_FORMAT_RGBA);
	selectedSprite = Sprite::createSprite(glm::ivec2(42, 42), glm::vec2(1, 1), &slectedTexture, &shaderProgram);
	selectedSprite->setNumberAnimations(1);
	selectedSprite->setAnimationSpeed(0, 1);
	selectedSprite->addKeyframe(0, glm::vec2(0.f, 0.f));
	selectedSprite->changeAnimation(0);
	selectedSprite->setPosition(glm::vec2(0, 0));
	selectedSprite->setPosition(glm::vec2(float(xOffset), float(yOffset)));

	CraftingTexture.loadFromFile("images/InventarioMenu.png", TEXTURE_PIXEL_FORMAT_RGBA);
	craftingMenu = Sprite::createSprite(glm::ivec2(147, 55), glm::vec2(1, 1), &CraftingTexture, &shaderProgram);
	craftingMenu->setNumberAnimations(1);
	craftingMenu->setAnimationSpeed(0, 1);
	craftingMenu->addKeyframe(0, glm::vec2(0.f, 0.f));
	craftingMenu->changeAnimation(0);
	craftingMenu->setPosition(glm::vec2(0, 0));
	craftingMenu->setPosition(glm::vec2(float(xOffset), float(yOffset)));

	tileMapDispl = tileMapPos;
	objectTexture.loadFromFile("images/objetosTilesheet.png", TEXTURE_PIXEL_FORMAT_RGBA);
	NumbersTexture.loadFromFile("images/numbers.png", TEXTURE_PIXEL_FORMAT_RGBA);
}

Sprite * InventarioUI::createObject(int objectId, ShaderProgram &shaderProgram)
{
	Sprite *sprite;

	sprite = Sprite::createSprite(glm::ivec2(32, 32), glm::vec2(0.125, 0.33), &objectTexture, &shaderProgram);
	sprite->setNumberAnimations(1);
	sprite->setAnimationSpeed(0, 1);
	int offset = Cjt_Objetos::instance().getObjeto(objectId).getTextureId();
	float yCoord = (offset / 8) * 0.33;
	float xCoord = (offset % 8)* 0.125f;
	sprite->addKeyframe(0, glm::vec2(xCoord, yCoord));
	sprite->changeAnimation(0);
	return sprite;
}
Sprite * InventarioUI::createNumber(int number, ShaderProgram & shaderProgram)
{
	Sprite *sprite;

	sprite = Sprite::createSprite(glm::ivec2(12, 12), glm::vec2(0.1, 1), &NumbersTexture, &shaderProgram);
	sprite->setNumberAnimations(1);
	sprite->setAnimationSpeed(0, 1);
	float xCoord = number *0.1f;
	sprite->addKeyframe(0, glm::vec2(xCoord, 0.f));
	sprite->changeAnimation(0);
	return sprite;
}
void InventarioUI::possible_Craft(int id) {
	delete craftingSprites[0];
	delete craftingSprites[1];
	delete craftingSprites[2];
	craftingSprites[0] = NULL;
	craftingSprites[1] = NULL;
	craftingSprites[2] = NULL;
	vector<objeto> ret;
	for (int i = 0; i < Cjt_Objetos::instance().objetos.size(); ++i) {
		if (Cjt_Objetos::instance().getObjeto(i).is_crafteable() && Cjt_Objetos::instance().getObjeto(i).getObjetoNecesario() == id) {
			int tengo = 0;
			vector<pair<int, int>> dependencias = Cjt_Objetos::instance().getObjeto(i).getDependencias();
			for (int j = 0; j < dependencias.size(); ++j) {
				for (int k = 0; k < inventario.size(); ++k) {
					if (inventario[k].first == dependencias[j].first &&
						inventario[k].second >= dependencias[j].second) ++tengo;
				}
			}
			if (tengo == dependencias.size()) {
				ret.push_back(Cjt_Objetos::instance().getObjeto(i));
			}
		}
	}
	possibleCrafting = ret;
	cantidadesCrafting = vector<pair<Sprite*, pair<Sprite*,Sprite*>>>(possibleCrafting.size());
	if (possibleCrafting.size() > 0) {
		for (int i = 0; i < possibleCrafting.size(); ++i) {
			vector<pair<int, int>> itemsNecesarios = possibleCrafting[i].getDependencias();
			cantidadesCrafting[i].first = createNumber(1, shaderProgram);
			cantidadesCrafting[i].second.first = createNumber((itemsNecesarios[0].second) % 10, shaderProgram);
			cantidadesCrafting[i].second.second = createNumber((itemsNecesarios[1].second) % 10, shaderProgram);
		}
	}
	cout << "puedo craftear: " << possibleCrafting.size() << " objetos" << endl;
}

bool InventarioUI::isFull()
{
	for (int i = 0; i < inventario.size(); ++i) {
		if (inventario[i].first == -1) return false;
	}
	return true;
}

bool InventarioUI::isCrafteando()
{
	return crafteando;
}

void InventarioUI::update(int deltaTime, glm::vec2 posPlayer)
{
	sprite->update(deltaTime);
	if (posPlayer.x > 4000 - 1024 / 2) posPlayer.x = 4000 - 1024 / 2;
	if (posPlayer.x < 1024 / 2) posPlayer.x = 1024 / 2;
	sprite->setPosition(glm::vec2(float(posPlayer.x-xOffset), float(posPlayer.y - yOffset)));
	posInventario = glm::vec2(float(posPlayer.x - xOffset), float(posPlayer.y - yOffset));
	selectedSprite->setPosition(glm::vec2(posInventario.x + seleccionado * 47, 2 + posInventario.y));
	craftingMenu->setPosition(glm::vec2(float(posPlayer.x - xOffset +2), float(posPlayer.y + 55)));
	for (int i = 0; i < 20; ++i) {
		if (inventario[i].first > -1) {
			if (i < 10) {
				inventoryObjects[i]->setPosition(glm::vec2(posInventario.x + i * 47 + 5, 2 + posInventario.y + 5));
				cantidadesInventario[i].first->setPosition(glm::vec2(posInventario.x + i * 47 + 19, 2 + posInventario.y + 30));
				cantidadesInventario[i].second->setPosition(glm::vec2(21 + posInventario.x + i * 47 + 10, 2 + posInventario.y + 30));
			}
			else {
				inventoryObjects[i]->setPosition(glm::vec2(posInventario.x + (i-10) * 47 + 5, 54 + posInventario.y + 5));
				cantidadesInventario[i].first->setPosition(glm::vec2(posInventario.x + (i - 10) * 47 + 19, 54 + posInventario.y + 30));
				cantidadesInventario[i].second->setPosition(glm::vec2(21 + posInventario.x + (i - 10) * 47 + 10, 54 + posInventario.y + 30));
			}
		}
	}
	if (crafteando) {
		if (possibleCrafting.size() > 0) {
			craftingSprites[0] = createObject(possibleCrafting[craftingMostrando].getId(), shaderProgram);
			craftingSprites[0]->setPosition(glm::vec2(float(posPlayer.x - xOffset + 13), float(posPlayer.y + 65)));
			craftingSprites[1] = createObject(possibleCrafting[craftingMostrando].getDependencias()[0].first, shaderProgram);
			craftingSprites[1]->setPosition(glm::vec2(float(posPlayer.x - xOffset + 65), float(posPlayer.y + 67)));
			craftingSprites[2] = createObject(possibleCrafting[craftingMostrando].getDependencias()[1].first, shaderProgram);
			craftingSprites[2]->setPosition(glm::vec2(float(posPlayer.x - xOffset + 110), float(posPlayer.y + 67)));
			cantidadesCrafting[craftingMostrando].first->setPosition(glm::vec2(float(posPlayer.x - xOffset + 13), float(posPlayer.y + 65)));
			cantidadesCrafting[craftingMostrando].second.first->setPosition(glm::vec2(float(posPlayer.x - xOffset + 65), float(posPlayer.y + 67)));
			cantidadesCrafting[craftingMostrando].second.second->setPosition(glm::vec2(float(posPlayer.x - xOffset + 110), float(posPlayer.y + 67)));
			if (Game::instance().getSpecialKey(GLUT_KEY_DOWN)) {
				downPulsado = true;
			}
			if(downPulsado && !Game::instance().getSpecialKey(GLUT_KEY_DOWN)){
				downPulsado = false;
				if (craftingMostrando > 0) {
					--craftingMostrando;
				}
			}
			if (Game::instance().getSpecialKey(GLUT_KEY_UP)) {
				upPulsado = true;
			}
			if (upPulsado && !Game::instance().getSpecialKey(GLUT_KEY_UP)) {
				upPulsado = false;
				if (craftingMostrando < possibleCrafting.size() - 1) {
					++craftingMostrando;
				}
			}
		}
	}
}

void InventarioUI::render()
{
	sprite->render();
	if (crafteando) {
		craftingMenu->render();
		if (craftingSprites[0] != NULL) {
			craftingSprites[0]->render();
			cantidadesCrafting[craftingMostrando].first->render();
		}
		if (craftingSprites[1] != NULL) {
			craftingSprites[1]->render();
			cantidadesCrafting[craftingMostrando].second.first->render();
		}
		if (craftingSprites[2] != NULL) {
			craftingSprites[2]->render();
			cantidadesCrafting[craftingMostrando].second.second->render();
		}
	}
	selectedSprite->render();
	int i = 0;
	for each (Sprite *s in inventoryObjects)
	{

		if (s != NULL) {
			s->render();
			cantidadesInventario[i].first->render();
			cantidadesInventario[i].second->render();
		}
		++i;
	}

}

void InventarioUI::setPosition(const glm::vec2 & pos)
{
	glm::vec2 auxPos = pos;
	posInventario = auxPos;
}

glm::vec2 InventarioUI::getPosition()
{
	return glm::vec2();
}

void InventarioUI::setObjectToInventoryPos(int objectId)
{
	if (objectId == 1) objectId = 0; // Debido a los dos tipos de tierra, con o sin cesped
	bool spaceFound = false;
	int inventorySpace = -1;
	int firstSpaceEmpty = -1;
	for (int i = 0; i < inventario.size(); ++i) {
		if (inventario[i].first == -1 && firstSpaceEmpty == -1) {
			firstSpaceEmpty = i;
			spaceFound = true;// he encontrado donde va
		}
		if (inventario[i].first == objectId) {
			spaceFound = true;
			inventorySpace = i;
		}
	}
	//va en un sitio nuevo
	if (inventorySpace == -1) {
		inventorySpace = firstSpaceEmpty;
		if (inventorySpace == -1) return;
		inventoryObjects[inventorySpace] = createObject(objectId, shaderProgram);
	}
	//TODO:Que pasa si no encuentra ninguno vacio?
	inventario[inventorySpace].first = objectId;
	inventario[inventorySpace].second += 1;
	delete cantidadesInventario[inventorySpace].first;
	delete cantidadesInventario[inventorySpace].second;
	if (inventario[inventorySpace].second > 9) {
		cantidadesInventario[inventorySpace].first = createNumber((inventario[inventorySpace].second)/10, shaderProgram);
		cantidadesInventario[inventorySpace].second = createNumber((inventario[inventorySpace].second)%10	, shaderProgram);
	}
	else {
		cantidadesInventario[inventorySpace].second = createNumber(inventario[inventorySpace].second, shaderProgram);
		cantidadesInventario[inventorySpace].first = createNumber(0, shaderProgram);
	}
}

void InventarioUI::setSelectedItem(int inventorySpace)
{
	seleccionado = inventorySpace;
}

int InventarioUI::getItemId(int x)
{
	return inventario[x].first;
}

int InventarioUI::getSize()
{
	return inventario.size();
}

int InventarioUI::getCantidad(int x)
{
	return inventario[x].first;
}

void InventarioUI::craftearObjeto(int id)
{
	if (possibleCrafting[craftingMostrando].getObjetoNecesario() != id) return;
	if (possibleCrafting.size() == 0)return;
	objeto obj = possibleCrafting[craftingMostrando];
	for (int i = 0; i < obj.getDependencias().size(); ++i) {
		for (int j = 0; j < inventario.size(); ++j) {
			if (inventario[j].first == obj.getDependencias()[i].first) InventarioUI::modificarCantidad(j, -obj.getDependencias()[i].second);
		}
	}
	setObjectToInventoryPos(obj.getId());
	delete craftingSprites[0];
	delete craftingSprites[1];
	delete craftingSprites[2];
	craftingSprites[0] = NULL;
	craftingSprites[1] = NULL;
	craftingSprites[2] = NULL;
	possible_Craft(id);
	craftingMostrando = 0;
}

void InventarioUI::modificarCantidad(int posicion, int cantidad)
{
	inventario[posicion].second += cantidad;
	if (inventario[posicion].second == 0) {
		inventario[posicion].first = -1;
		inventoryObjects[posicion] = NULL;
	}
	delete cantidadesInventario[posicion].first;
	delete cantidadesInventario[posicion].second;
	if (inventario[posicion].second > 9) {
		cantidadesInventario[posicion].first = createNumber((inventario[posicion].second) / 10, shaderProgram);
		cantidadesInventario[posicion].second = createNumber((inventario[posicion].second) % 10, shaderProgram);
	}
	else {
		cantidadesInventario[posicion].second = createNumber(inventario[posicion].second, shaderProgram);
		cantidadesInventario[posicion].first = createNumber(0, shaderProgram);
	}
}

void InventarioUI::setCantidad(int posicion, int cantidad)
{
	inventario[posicion].second = cantidad;
	if (inventario[posicion].second == 0) {
		inventario[posicion].first = -1;
		inventoryObjects[posicion] = NULL;
	}
	delete cantidadesInventario[posicion].first;
	delete cantidadesInventario[posicion].second;
	if (inventario[posicion].second > 9) {
		cantidadesInventario[posicion].first = createNumber((inventario[posicion].second) / 10, shaderProgram);
		cantidadesInventario[posicion].second = createNumber((inventario[posicion].second) % 10, shaderProgram);
	}
	else {
		cantidadesInventario[posicion].second = createNumber(inventario[posicion].second, shaderProgram);
		cantidadesInventario[posicion].first = createNumber(0, shaderProgram);
	}
}

void InventarioUI::swapItems(int itemACambiar, int destino)
{
	if (inventario[itemACambiar].first != -1) {
		pair<int, int> aux = inventario[destino];
		Sprite *sAux = inventoryObjects[destino];
		inventoryObjects[destino] = inventoryObjects[itemACambiar];
		inventoryObjects[itemACambiar] = sAux;
		inventario[destino] = inventario[itemACambiar];
		inventario[itemACambiar] = aux;

		pair<Sprite*, Sprite*> aux2 = cantidadesInventario[destino];
		cantidadesInventario[destino] = cantidadesInventario[itemACambiar];
		cantidadesInventario[itemACambiar] = aux2;
	}
}

void InventarioUI::swapCrafteando()
{
	crafteando = !crafteando;

}
