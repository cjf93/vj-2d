#ifndef _PLAYER_INCLUDE
#define _PLAYER_INCLUDE


#include "Sprite.h"
#include "TileMap.h"
#include "objeto.h"
#include "Slug.h"
#include "Arpia.h"
#include "InventarioUI.h"
#include <time.h>  
#include "HPUI.h"


// Player is basically a Sprite that represents the player. As such it has
// all properties it needs to track its movement, jumping, and collisions.


class Player
{

public:
	void init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram);
	void update(int deltaTime, vector<Arpia*>& Arpias, vector<Slug*>& Slugs);
	void render();
	bool is_behind(int id);
	void setTileMap(TileMap *tileMap);
	void setInventory(InventarioUI *inventario);
	void setHPUI(HPUI *HPUI);
	void setPosition(const glm::vec2 &pos);
	glm::vec2 getPosition();
	
private:
	bool bJumping;
	pair<int, int> spawnPoint;
	void init_inventario();
	clock_t timeActuar = 0;
	bool colisionMonstruo(Slug *slug);
	bool colisionMonstruo(Arpia *slug);
	glm::ivec2 tileMapDispl, posPlayer;
	int jumpAngle, startY;
	void click(vector<Arpia*>& arpias, vector<Slug*>& slugs);
	void anadirObjeto(int objId, int num);
	Texture spritesheet;
	Sprite *sprite;
	Texture spritesheetEspada;
	Sprite *spriteEspada;
	TileMap *map;
	vector<Arpia> Arpias;
	vector<Slug> Slugs;
	InventarioUI *inventarioUI;
	HPUI *vida;
	int seleccionado;
	clock_t timeShield;
	bool shield;
	clock_t currentTime;
	clock_t currentTimeAct;
	bool cPulsado;
};


#endif // _PLAYER_INCLUDE


