#pragma once
#include <iostream>
#include <vector>
#include "objeto.h"
#include "reader.h"
using namespace std;

class Cjt_Objetos
{
public:
	Cjt_Objetos();
	static Cjt_Objetos &instance()
	{
		static Cjt_Objetos G;

		return G;
	}
	void leerObjetos();
	objeto getObjeto(int x);
	int getSize();
	vector<objeto> objetos;
	~Cjt_Objetos();

};

