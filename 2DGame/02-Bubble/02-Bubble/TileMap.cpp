#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "TileMap.h"
#include "Game.h"
#include "Cjt_Objetos.h"
#include <time.h>


using namespace std;


TileMap *TileMap::createTileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program)
{
	TileMap *map = new TileMap(levelFile, minCoords, program);
	
	return map;
}


TileMap::TileMap(const string &levelFile, const glm::vec2 &minCoords, ShaderProgram &program)
{
	loadLevel(levelFile);
	MyminCoords = minCoords;
	Myprogram = program;
	prepareArrays(minCoords, program);
}

TileMap::~TileMap()
{
	if (map != NULL)
		delete map;
}


void TileMap::render() const
{
	glEnable(GL_TEXTURE_2D);
	tilesheet.use();
	glBindVertexArray(vao);
	glEnableVertexAttribArray(posLocation);
	glEnableVertexAttribArray(texCoordLocation);
	glDrawArrays(GL_TRIANGLES, 0, 6 * nTiles);
	glDisable(GL_TEXTURE_2D);

}

void TileMap::free()
{
	//sglDeleteBuffers(1, &vbo);
}

bool TileMap::loadLevel(const string &levelFile)
{

	ifstream fin;
	string line, tilesheetFile;
	stringstream sstream;
	char tile;

	fin.open(levelFile.c_str());
	if (!fin.is_open())
		return false;
	getline(fin, line);
	if (line.compare(0, 7, "TILEMAP") != 0)
		return false;
	getline(fin, line);
	sstream.str(line);
	sstream >> mapSize.x >> mapSize.y;
	getline(fin, line);
	sstream.str(line);
	sstream >> tileSize >> blockSize;
	getline(fin, line);
	sstream.str(line);
	sstream >> tilesheetFile;
	tilesheet.loadFromFile(tilesheetFile, TEXTURE_PIXEL_FORMAT_RGBA);
	tilesheet.setWrapS(GL_CLAMP_TO_EDGE);
	tilesheet.setWrapT(GL_CLAMP_TO_EDGE);
	tilesheet.setMinFilter(GL_NEAREST);
	tilesheet.setMagFilter(GL_NEAREST);
	getline(fin, line);
	sstream.str(line);
	sstream >> tilesheetSize.x >> tilesheetSize.y;
	tileTexSize = glm::vec2(1.f / tilesheetSize.x, 1.f / tilesheetSize.y);

	map = new int[mapSize.x * mapSize.y];
	for (int j = 0; j<mapSize.y; j++)
	{
		getline(fin, line);

		vector<string> s = Split(line, ',', mapSize.x);
		for (int i = 0; i<mapSize.x; i++)
		{
			//fin.get(tile);
			if (s[i] == "-1")
				map[j*mapSize.x + i] = -1;
			else
				map[j*mapSize.x + i] = getIntValue(s[i]);
		}
		//fin.get(tile);
#ifndef _WIN32
		//fin.get(tile);
#endif
	}
	fin.close();
	return true;
}

void TileMap::prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program)	
{
	//vector<float> vertices;
	int tile = 0;
	nTiles = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	for (int j = 0; j<mapSize.y; j++)
	{
		for (int i = 0; i<mapSize.x; i++)
		{
			tile = map[j * mapSize.x + i];
			if (tile != -1)
			{
				// Non-empty tile
				nTiles++;
				posTile = glm::vec2(minCoords.x + i * tileSize, minCoords.y + j * tileSize);
				texCoordTile[0] = glm::vec2(float((tile) % 16) / tilesheetSize.x, float((tile) / 16) / tilesheetSize.y);
				texCoordTile[1] = texCoordTile[0] + tileTexSize;
				//texCoordTile[0] += halfTexel;
				texCoordTile[1] -= halfTexel;
				//First triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				//Second triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
			}
		}
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4 * sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4 * sizeof(float), (void *)(2 * sizeof(float)));
	
}

void TileMap::updateArrays(const glm::vec2 & minCoords, glm::vec2 renderTile, ShaderProgram & program, int idObj, int offset)
{
	int i = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	bool borrado = false;
	while( i< vertices.size() && !borrado) {
		if (vertices[i]==renderTile.x*tileSize && vertices[i+1] == renderTile.y*tileSize) {
			for (int j = 0; j < 24; ++j) {
				vertices.erase(vertices.begin()+i);
			}
			--nTiles;
			borrado = true;
		}
		i=i+24;
	}
	if (!borrado) {
		++nTiles;
		glm::vec2 posTile, texCoordTile[2], halfTexel;
		halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
		int tile = idObj*16 + offset;
		posTile = glm::vec2(minCoords.x +renderTile.x * tileSize, minCoords.y + renderTile.y * tileSize);
		texCoordTile[0] = glm::vec2(float((tile) % 16) / tilesheetSize.x, float((tile) / 16) / tilesheetSize.y);
		texCoordTile[1] = texCoordTile[0] + tileTexSize;
		//texCoordTile[0] += halfTexel;
		texCoordTile[1] -= halfTexel;
		//First triangle
		vertices.push_back(posTile.x); vertices.push_back(posTile.y);
		vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
		vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
		vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
		vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
		vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
		//Second triangle
		vertices.push_back(posTile.x); vertices.push_back(posTile.y);
		vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
		vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
		vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
		vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
		vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
	}
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4 * sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4 * sizeof(float), (void *)(2 * sizeof(float)));
}

vector<string> TileMap::Split(string s, char split, int size) {
	vector<string> result(size);
	int lastIndex = 0;
	for (int i = 0; i < s.size(); ++i) {
		if (s[i] != split) {
			result[lastIndex] += s[i];
		}
		else {
			++lastIndex;
		}
	}
	return result;
}

int TileMap::getIntValue(string s) {
	int result = 0;
	const char * c = s.c_str();
	char * pEnd;
	result = strtol(c, &pEnd, 10);
	//result = atoi(s.c_str);
	return result;
}

// Collision tests for axis aligned bounding boxes.
// Method collisionMoveDown also corrects Y coordinate if the box is
// already intersecting a tile below.

int TileMap::getPos(glm::ivec2 pos)
{
	return map[pos.x /tileSize + (pos.y / tileSize +2) * mapSize.x] ;
}

bool TileMap::colocarObjeto(int posX, int posY, int objId)
{
	posX = ((posX + Game::instance().scene.getDespCamX()) / tileSize) + SCREEN_WIDTH / 16 - 96;
	posY = ((posY + Game::instance().scene.getDespCamY()) / tileSize) - SCREEN_HEIGHT / 16 + 24;
	if (map[posY*mapSize.x + posX] == -1) {
		if (objId < 9) {
			map[posY*mapSize.x + posX] = (objId * 16);
			updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 3);
			return true;
		}
		else if (objId >= 9) {
			switch (objId)
			{
			case 9: //horno
				if (map[posY * mapSize.x + posX + 1] == -1 && map[(posY - 1) * mapSize.x + posX] == -1 && map[(posY - 1) * mapSize.x + posX + 1] == -1) {
					map[posY*mapSize.x + posX] = (objId * 16);
					map[posY*mapSize.x + posX] = (objId * 16 + 2);
					map[posY * mapSize.x + posX + 1] = (objId * 16 + 3);
					map[(posY - 1) * mapSize.x + posX] = (objId * 16);
					map[(posY - 1) * mapSize.x + posX + 1] = (objId * 16 + 1);
					updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 2);
					updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, objId, 3);
					updateArrays(MyminCoords, glm::ivec2(posX, posY - 1), Myprogram, objId, 0);
					updateArrays(MyminCoords, glm::ivec2(posX + 1, posY - 1), Myprogram, objId, 1);
					return true;
				}
				break;
			case 10://mesa
				if (map[posY * mapSize.x + posX + 1] == -1) {
					map[posY*mapSize.x + posX] = (objId * 16);
					map[posY*mapSize.x + posX + 1] = (objId * 16 + 1);
					updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 0);
					updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, objId, 1);
					return true;
				}
				break;
			case 11: //cama
				if (map[posY * mapSize.x + posX + 1] == -1) {
					map[posY*mapSize.x + posX] = (objId * 16);
					map[posY*mapSize.x + posX + 1] = (objId * 16 + 1);
					updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 0);
					updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, objId, 1);
					return true;
				}
				break;

			case 12://naranja
				if (map[posY * mapSize.x + posX + 1] == -1) {
					map[(posY - 1) * mapSize.x + posX] = (objId * 16);
					map[(posY) * mapSize.x + posX] = (objId * 16+1);
					updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 1);
					updateArrays(MyminCoords, glm::ivec2(posX , posY-1), Myprogram, objId, 0);
					portalNPos= glm::ivec2(posX, (posY - 1));
					return true;
				}
				break;
			case 13://azul
				if (map[posY * mapSize.x + posX + 1] == -1) {
					map[(posY - 1) * mapSize.x + posX] = (objId * 16);
					map[(posY)* mapSize.x + posX] = (objId * 16 + 1);
					updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, objId, 1);
					updateArrays(MyminCoords, glm::ivec2(posX , posY-1), Myprogram, objId, 0);
					portalAPos = glm::ivec2(posX, (posY - 1));
					return true;
				}
				break;
			default:
				break;
			}
		}
	}
	return false;
}

objeto TileMap::extraer(int posX, int posY, objeto obj)
{
	
	posX = ((posX + Game::instance().scene.getDespCamX()) / tileSize) + SCREEN_WIDTH / 16 - 96;
	posY = ((posY + Game::instance().scene.getDespCamY()) / tileSize) - SCREEN_HEIGHT / 16 + 24;
	if (map[posY*mapSize.x + posX] != -1) {
		int id = (map[posY*mapSize.x + posX]) / 16;
		if (obj.getDureza() >= Cjt_Objetos::instance().getObjeto(id).getDureza()) {
			 if (id >= 9) {
				switch (id)
				{
				case 9: //horno
					if (map[posY*mapSize.x + posX] == 144) {
						updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX + 1, posY + 1), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX, posY + 1), Myprogram, -1, 0);
						map[posY*mapSize.x + posX + 1] = -1;
						map[(posY + 1) *mapSize.x + posX + 1] = -1;
						map[(posY + 1)*mapSize.x + posX] = -1;
					}
					else if (map[posY*mapSize.x + posX] == 145) {
						updateArrays(MyminCoords, glm::ivec2(posX - 1, posY), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX - 1, posY + 1), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX, posY + 1), Myprogram, -1, 0);
						map[posY*mapSize.x + posX - 1] = -1;
						map[(posY + 1)*mapSize.x + posX] = -1;
						map[(posY + 1)*mapSize.x + posX - 1] = -1;
					}
					else if (map[posY*mapSize.x + posX] == 146) {
						updateArrays(MyminCoords, glm::ivec2(posX, posY - 1), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX + 1, posY - 1), Myprogram, -1, 0);
						map[(posY - 1)*mapSize.x + posX] = -1;
						map[posY*mapSize.x + posX + 1] = -1;
						map[(posY - 1)*mapSize.x + posX + 1] = -1;
					}
					else if (map[posY*mapSize.x + posX] == 147) {
						updateArrays(MyminCoords, glm::ivec2(posX - 1, posY), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX - 1, posY - 1), Myprogram, -1, 0);
						updateArrays(MyminCoords, glm::ivec2(posX, posY - 1), Myprogram, -1, 0);
						map[posY*mapSize.x + posX - 1] = -1;
						map[(posY - 1)*mapSize.x + posX] = -1;
						map[(posY - 1)*mapSize.x + posX - 1] = -1;
					}
					break;
				case 10://mesa
					if (map[posY*mapSize.x + posX + 1] == map[posY*mapSize.x + posX] + 1) {
						updateArrays(MyminCoords, glm::ivec2(posX + 1, posY), Myprogram, -1, 0);
						map[posY*mapSize.x + posX + 1] = -1;
					}
					else {
						updateArrays(MyminCoords, glm::ivec2(posX - 1, posY), Myprogram, -1, 0);
						map[posY*mapSize.x + posX - 1] = -1;
					}
					break;
				case 11: //cama
					if (map[posY*mapSize.x + posX + 1] == map[posY*mapSize.x + posX] + 1) {
						updateArrays(MyminCoords, glm::ivec2(posX+1, posY), Myprogram, -1, 0);
						map[posY*mapSize.x + posX + 1] = -1;
					}
					else {
						updateArrays(MyminCoords, glm::ivec2(posX-1, posY), Myprogram, -1, 0);
						map[posY*mapSize.x + posX - 1] = -1;
					}
					break;
				case 12: 
					if (map[(posY+1)*mapSize.x + posX] == map[posY*mapSize.x + posX] + 1) {
						updateArrays(MyminCoords, glm::ivec2(posX, posY+1), Myprogram, -1, 0);
						map[(posY+1)*mapSize.x + posX] = -1;
					}
					else {
						updateArrays(MyminCoords, glm::ivec2(posX, posY-1), Myprogram, -1, 0);
						map[(posY-1)*mapSize.x + posX ] = -1;
					}
					portalNPos = glm::ivec2(-1, -1);
					break;
				case 13:
					if (map[(posY+1)*mapSize.x + posX] == map[posY*mapSize.x + posX] + 1) {
						updateArrays(MyminCoords, glm::ivec2(posX, posY+1), Myprogram, -1, 0);
						map[(posY+1)*mapSize.x + posX] = -1;
					}
					else {
						updateArrays(MyminCoords, glm::ivec2(posX, posY-1), Myprogram, -1, 0);
						map[(posY-1)*mapSize.x + posX] = -1;
					}
					portalAPos = glm::ivec2(-1, -1);
					break;
				default:
					break;
				}
			}
			obj = Cjt_Objetos::instance().getObjeto(id);
			map[posY*mapSize.x + posX] = -1;
			updateArrays(MyminCoords, glm::ivec2(posX, posY), Myprogram, -1, 0);			
			return obj;

		}
	}
	return objeto();
}

bool TileMap::is_empty(int posX, int posY)
{
	posX = (posX / tileSize) - 2;
	posY = (posY / tileSize) - 1;
	return map[posY*mapSize.x + posX]==-1;
}

int TileMap::alturaSuelo(glm::ivec2 posArpia)
{
	int altura = 0;
	int posX = posArpia.x / tileSize;
	int posY = posArpia.y / tileSize;
	int i=1;
	while (altura == 0) {
		if (map[(posY + i)*mapSize.x + posX] != -1) altura = ((posY + i)*mapSize.x + posX)/ mapSize.x;
		++i;
	}
	return altura;
}

glm::ivec2 TileMap::getPortalApos()
{
	return portalAPos;
}

glm::ivec2 TileMap::getPortalNpos()
{
	return portalNPos;
}

bool TileMap::collisionMoveLeft(const glm::ivec2 &pos, const glm::ivec2 &size) const
{
	int x, y0, y1;
	
	x = pos.x / tileSize;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for(int y=y0; y<=y1; y++)
	{

		if(!(map[y*mapSize.x + x] == -1 || map[y*mapSize.x + x] >= LINIANOCOLLIDE * tilesheetSize.x))
			return true;
	}
	
	return false;
}

bool TileMap::collisionMoveRight(const glm::ivec2 &pos, const glm::ivec2 &size) const
{
	int x, y0, y1;
	
	x = (pos.x + size.x - 1) / tileSize;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for(int y=y0; y<=y1; y++)
	{
		if (!(map[y*mapSize.x + x] == -1 || map[y*mapSize.x + x] >= LINIANOCOLLIDE * tilesheetSize.x))
			return true;
	}
	
	return false;
}

bool TileMap::collisionMoveUp(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY) const
{
	int x0, x1, y;
	
	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	y = (pos.y ) / tileSize;
	for(int x=x0; x<=x1; x++)
	{
		if (!(map[y*mapSize.x + x] == -1 || map[y*mapSize.x + x] >= LINIANOCOLLIDE * tilesheetSize.x))
		{

				*posY = tileSize * y - size.y;
				return true;
		}
	}
	
	return false;
}

bool TileMap::collisionMoveDown(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY) const
{
	int x0, x1, y;
	
	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	y = (pos.y + size.y - 1) / tileSize;
	for(int x=x0; x<=x1; x++)
	{
		if (!(map[y*mapSize.x + x] == -1 || map[y*mapSize.x + x] >= LINIANOCOLLIDE * tilesheetSize.x))
		{
			if(*posY - tileSize * y + size.y <= 8)
			{
				*posY = tileSize * y - size.y;
				return true;
			}
		}
	}
	
	return false;
}






























