/*#include "cSound.h"
LPALEFFECTF alEffectf;
LPALEFFECTFV alEffectfv;
LPALEFFECTI alEffecti;
LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots;
LPALGENEFFECTS alGenEffects;
LPALDELETEEFFECTS alDeleteEffects;
float sound_gain = 0.8;
float song_gain = 0.5;

cSound::cSound(void)
{
}

cSound::~cSound(void)
{
}

void cSound::init()
{  
	// Init openAL
    alutInit(0, NULL);
    // Clear Error Code (so we can catch any new errors) 
    alGetError();

    // Create the buffers
    alGenBuffers(NUM_BUFFERS, buffers);
    if ((error = alGetError()) != AL_NO_ERROR)
    {
      printf("alGenBuffers : %d", error);
    }

	// Generate the sources
    alGenSources(NUM_SOURCES, source);
    if ((error = alGetError()) != AL_NO_ERROR)
    {
      printf("alGenSources : %d", error);
	}


	pos = 0;
	initSong("Final Boss Dialog.wav");
	initSong("silence-of-the-forest.wav");
	initSong("silence-of-the-forest_inv.wav");
	initSong("Morlia_gallery.wav");
	initSong("Morlia_gallery_inv.wav");
	initSound("MM_Land.wav");
	initSound("MM_Death.wav");
	initSound("breakblock.wav");
	initSound("splash.wav");
	initSound("bat_sound.wav");

	EAXREVERBPROPERTIES eaxUnderwater = REVERB_PRESET_UNDERWATER;
	// The EFX Extension includes support for global effects, such as Reverb.  To use a global effect,
		// you need to create an Auxiliary Effect Slot to store the Effect ...
	bEffectCreated = AL_FALSE;
    //if (CreateAuxEffectSlot(&EffectSlot))
	//{
			// Once we have an Auxiliary Effect Slot, we can generate an Effect Object, set its Type
			// and Parameter Values, and then load the Effect into the Auxiliary Effect Slot ...
			//if (CreateEffect(&Effect, AL_EFFECT_EAXREVERB))
			//{
			//	bEffectCreated = AL_TRUE;
			//}
			//else
			//{
			//	printf("Failed to Create an EAX Reverb Effect\n");
			//}

			//if (bEffectCreated)
			//{		
			//    // Reverb Preset is stored in legacy format, use helper function to convert to EFX EAX Reverb
			//    ConvertReverbParameters(&eaxUnderwater, &efxReverb);
			//	
			//    // Set the Effect parameters
			//    if (!SetEFXEAXReverbProperties(&efxReverb, Effect))
			//	    printf("Failed to set Reverb Parameters\n");

			//    // Load Effect into Auxiliary Effect Slot
			//    alAuxiliaryEffectSloti(EffectSlot, AL_EFFECTSLOT_EFFECT, Effect);

			//    // Enable (non-filtered) Send from Source to Auxiliary Effect Slot
			//    alSource3i(source[0], AL_AUXILIARY_SEND_FILTER, EffectSlot, 0, AL_FILTER_NULL);
	        //}
    //}

	//source
	ALvoid alSourcefv(ALuint source,ALenum pname,ALfloat *values);
	//alSourcefv (source[0], AL_POSITION, sourcePos);
    //alSourcefv (source[0], AL_VELOCITY, sourceVel);
    //alSourcefv (source[0], AL_DIRECTION, sourceOri);

	//listener
	ALvoid alListenerfv(ALenum pname,ALfloat *values);
	//(AL_POSITION,listenerPos);
    //alListenerfv(AL_VELOCITY,listenerVel);
    //alListenerfv(AL_ORIENTATION,listenerOri);


}

void cSound::play(ALbyte *sound)
{
	int num_source = sourcelist.at(sound);
	alSourcePlay(source[num_source]);
}

void cSound::pause(ALbyte *source)
{
	int num_source = sourcelist.at(source);
    alSourcePause(source[num_source]);
}

void cSound::finish()
{
	alDeleteSources(NUM_SOURCES, source);
    alDeleteBuffers(NUM_BUFFERS, buffers);
	alutExit();
}

void cSound::modify_song_gain(ALbyte* sound, float value)
{
	song_gain += value;
	int num_source = sourcelist.at(sound);
	alSourcef(source[num_source], AL_GAIN, song_gain);
}


void cSound::modify_sound_gain(ALbyte* sound, float value)
{
	sound_gain += value;
	int num_source = sourcelist.at(sound);
	alSourcef(source[num_source], AL_GAIN, sound_gain);
}

void cSound::initSound(ALbyte *sound)
{
sourcelist.insert(pair<ALbyte*, int>(sound, pos));

	alutLoadWAVFile(sound, &format, &data, &size, &freq, &loop);

	if ((error = alGetError()) != AL_NO_ERROR)
	{
	  printf("alutLoadWAVFile %s : %d", source, error);
	  //Delete Buffers
	  alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alBufferData(buffers[pos],format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		printf("alBufferData buffer 0 : %d", error);
		// Delete buffers
		alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alutUnloadWAV(format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
	  printf("alutUnloadWAV : %d", error);
	  // Delete buffers
	  alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alSourcei(source[pos], AL_BUFFER, buffers[pos]);
	alSourcef(source[pos], AL_GAIN, sound_gain);
    if ((error = alGetError()) != AL_NO_ERROR)
    {
      printf("alSourcei : %d", error);
    }
	++pos;
}

void cSound::initSong(ALbyte *song)
{
	sourcelist.insert(pair<ALbyte*, int>(song, pos));

	alutLoadWAVFile(song, &format, &data, &size, &freq, &loop);

	if ((error = alGetError()) != AL_NO_ERROR)
	{
	  printf("alutLoadWAVFile %s : %d", source, error);
	  //Delete Buffers
	  alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alBufferData(buffers[pos],format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		printf("alBufferData buffer 0 : %d", error);
		// Delete buffers
		alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alutUnloadWAV(format,data,size,freq);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
	  printf("alutUnloadWAV : %d", error);
	  // Delete buffers
	  alDeleteBuffers(NUM_BUFFERS, buffers);
	}

	alSourcei(source[pos], AL_BUFFER, buffers[pos]);
	alSourcef(source[pos], AL_GAIN, song_gain);
	alSourcei(source[pos], AL_LOOPING, AL_TRUE);
    if ((error = alGetError()) != AL_NO_ERROR)
    {
      printf("alSourcei : %d", error);
    }
	++pos;
}


ALboolean cSound::CreateAuxEffectSlot(ALuint *puiAuxEffectSlot)
{
	ALboolean bReturn = AL_FALSE;

	// Clear AL Error state
	alGetError();

	// Generate an Auxiliary Effect Slot
	//alGenAuxiliaryEffectSlots(1, puiAuxEffectSlot);

	if (alGetError() == AL_NO_ERROR)
		bReturn = AL_TRUE;

	return bReturn;
}

ALboolean cSound::CreateEffect(ALuint *puiEffect, ALenum eEffectType)
{
	ALboolean bReturn = AL_FALSE;

	if (puiEffect)
	{
		// Clear AL Error State
		alGetError();

		// Generate an Effect
		alGenEffects(1, puiEffect);
		if (alGetError() == AL_NO_ERROR)
		{
			// Set the Effect Type
			alEffecti(*puiEffect, AL_EFFECT_TYPE, eEffectType);
			if (alGetError() == AL_NO_ERROR)
				bReturn = AL_TRUE;
			else
				alDeleteEffects(1, puiEffect);
		}
	}

	return bReturn;
}


ALboolean cSound::SetEFXEAXReverbProperties(EFXEAXREVERBPROPERTIES *pEFXEAXReverb, ALuint uiEffect)
{
	ALboolean bReturn = AL_FALSE;

	if (pEFXEAXReverb)
	{
		// Clear AL Error code
		alGetError();

		alEffectf(uiEffect, AL_EAXREVERB_DENSITY, pEFXEAXReverb->flDensity);
		alEffectf(uiEffect, AL_EAXREVERB_DIFFUSION, pEFXEAXReverb->flDiffusion);
		alEffectf(uiEffect, AL_EAXREVERB_GAIN, pEFXEAXReverb->flGain);
		alEffectf(uiEffect, AL_EAXREVERB_GAINHF, pEFXEAXReverb->flGainHF);
		alEffectf(uiEffect, AL_EAXREVERB_GAINLF, pEFXEAXReverb->flGainLF);
		alEffectf(uiEffect, AL_EAXREVERB_DECAY_TIME, pEFXEAXReverb->flDecayTime);
		alEffectf(uiEffect, AL_EAXREVERB_DECAY_HFRATIO, pEFXEAXReverb->flDecayHFRatio);
		alEffectf(uiEffect, AL_EAXREVERB_DECAY_LFRATIO, pEFXEAXReverb->flDecayLFRatio);
		alEffectf(uiEffect, AL_EAXREVERB_REFLECTIONS_GAIN, pEFXEAXReverb->flReflectionsGain);
		alEffectf(uiEffect, AL_EAXREVERB_REFLECTIONS_DELAY, pEFXEAXReverb->flReflectionsDelay);
		alEffectfv(uiEffect, AL_EAXREVERB_REFLECTIONS_PAN, pEFXEAXReverb->flReflectionsPan);
		alEffectf(uiEffect, AL_EAXREVERB_LATE_REVERB_GAIN, pEFXEAXReverb->flLateReverbGain);
		alEffectf(uiEffect, AL_EAXREVERB_LATE_REVERB_DELAY, pEFXEAXReverb->flLateReverbDelay);
		alEffectfv(uiEffect, AL_EAXREVERB_LATE_REVERB_PAN, pEFXEAXReverb->flLateReverbPan);
		alEffectf(uiEffect, AL_EAXREVERB_ECHO_TIME, pEFXEAXReverb->flEchoTime);
		alEffectf(uiEffect, AL_EAXREVERB_ECHO_DEPTH, pEFXEAXReverb->flEchoDepth);
		alEffectf(uiEffect, AL_EAXREVERB_MODULATION_TIME, pEFXEAXReverb->flModulationTime);
		alEffectf(uiEffect, AL_EAXREVERB_MODULATION_DEPTH, pEFXEAXReverb->flModulationDepth);
		alEffectf(uiEffect, AL_EAXREVERB_AIR_ABSORPTION_GAINHF, pEFXEAXReverb->flAirAbsorptionGainHF);
		alEffectf(uiEffect, AL_EAXREVERB_HFREFERENCE, pEFXEAXReverb->flHFReference);
		alEffectf(uiEffect, AL_EAXREVERB_LFREFERENCE, pEFXEAXReverb->flLFReference);
		alEffectf(uiEffect, AL_EAXREVERB_ROOM_ROLLOFF_FACTOR, pEFXEAXReverb->flRoomRolloffFactor);
		alEffecti(uiEffect, AL_EAXREVERB_DECAY_HFLIMIT, pEFXEAXReverb->iDecayHFLimit);

		if (alGetError() == AL_NO_ERROR)
			bReturn = AL_TRUE;
	}

	return bReturn;
}*/
