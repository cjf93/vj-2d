#pragma once
#include <iostream>
#include <vector>
using namespace std;


class objeto
{
public:
	objeto();
	objeto(int id, int textureId, bool usable, bool consumible, bool crafteable, int dureza, int ataque, bool dropeable, bool mineria, int objetonecesario, vector<pair<int, int>> dependencias);
	int getId();
	int getTextureId();
	bool is_usable();
	bool is_consumible();
	bool is_crafteable();
	int getDureza();
	int getAtaque();
	bool is_dropeable();
	bool is_mineria();
	int getObjetoNecesario();
	vector<pair<int, int>> getDependencias();//id del objeto necesario, cantidad de ese objeto
	~objeto();

private:
	int id;
	int textureId;
	bool usable;
	bool consumible;
	bool crafteable;
	int dureza;
	int ataque;
	bool dropeable;
	bool mineria;
	int objetonecesario;
	vector<pair<int, int>> dependencias;
};

